#include "AnimationStation.h"

#include <cassert>

#include "TextFileReader.h"
#include "Image.h"
#include "Constants.h"

void AnimationStation::CreateAnimation(SDL_Renderer *pRen, const char* pFilename, 
    std::vector<Image>& timeline, SDL_Rect dst, int &timePerFrame)
{
    std::string animationVariables;
    std::string variable;
    size_t stringOffset;
    int timelineLength;
    int xOffset;
    int yOffset;
    SDL_Rect src;

    TextFileReader::ReadEntireTextToString(pFilename, animationVariables);

    //Set Image
    SDL_Texture *texture = SetTexture(pRen, animationVariables, "Image: ");
    assert(texture != nullptr);

    // Get Timeline Variables
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, "Timeline Length: ");
    TextFileReader::GetValueFromString(variable, 0, timelineLength);

    // Get how long each frame of the timeline should be shown for
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, "Time Per Frame: ");
    TextFileReader::GetValueFromString(variable, 0, timePerFrame);

    // Set Timeline
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, "Offset: ");
    stringOffset = TextFileReader::GetValueFromString(variable, 0, xOffset);
    TextFileReader::GetValueFromString(variable, stringOffset, yOffset);

    TextFileReader::GetRecFromString(animationVariables, src, (std::string)"Source: ");

    for (int i{ 0 }; i < timelineLength; ++i)
    {
        timeline.push_back(Image(texture, pRen, src, dst));
        src.x += xOffset;
        src.y += yOffset;
    }
}

void AnimationStation::CreateAnimationFromMultipleSources(SDL_Renderer * pRen, const char * pFilename, std::vector<Image>& timeline, SDL_Rect dst, int & timePerFrame)
{
    std::string animationVariables;
    std::string variable;
    int timelineLength;
    SDL_Rect src;

    TextFileReader::ReadEntireTextToString(pFilename, animationVariables);

    // Get Timeline Variables
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, "Timeline Length: ");
    TextFileReader::GetValueFromString(variable, 0, timelineLength);

    // Get how long each frame of the timeline should be shown for
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, "Time Per Frame: ");
    TextFileReader::GetValueFromString(variable, 0, timePerFrame);

    TextFileReader::GetRecFromString(animationVariables, src, (std::string)"Source: ");

    int i{ 0 };
    while ( i < timelineLength)
    {
        std::string imageToLoadName{ "Image" };
        imageToLoadName += std::to_string(i);
        imageToLoadName += ": ";
        SDL_Texture *texture = SetTexture(pRen, animationVariables, imageToLoadName);
        assert(texture != nullptr);
        timeline.push_back(Image(texture, pRen, src, dst));
        ++i;
    }
}

SDL_Texture* AnimationStation::SetTexture(SDL_Renderer *pRen,  std::string& animationVariables, std::string nameOfImageHeader )
{
    std::string variable;
    variable = TextFileReader::GetSelectedDataFromString(animationVariables, nameOfImageHeader);
    std::string image = "Assets/Images/";
    image += variable;
    const char *cstr = image.c_str();
    return IMG_LoadTexture(pRen, cstr);
}
