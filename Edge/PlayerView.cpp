#include "PlayerView.h"

#include "Image.h"
#include "CollisionShapeAABB.h"
#include "TextFileReader.h"

// Player view gets data from the player controller, it itself knows nothing about the controller or the model
PlayerView::PlayerView(SDL_Renderer* pRen, std::string variables)
    : m_pRen(pRen)
    , m_pPlayer(VARIABLES("Player"), m_pRen)
{
    Init(variables);
}

void PlayerView::Init(std::string variables)
{
    m_pCollider = new CollisionShapeAABB(m_pPlayer.GetDstRect());

    std::string specificVariable;
    size_t offset;
    specificVariable = TextFileReader::GetSelectedDataFromString(variables, "Sprite Sheet Position: ");
    offset = TextFileReader::GetValueFromString(specificVariable, 0, m_srcX);
    TextFileReader::GetValueFromString(specificVariable, offset, m_srcY);

    specificVariable = TextFileReader::GetSelectedDataFromString(variables, "Animation Timeline Length: ");
    TextFileReader::GetValueFromString(specificVariable, 0, m_timelineLength);
}

PlayerView::~PlayerView()
{
    delete m_pCollider;
    m_pCollider = nullptr;
}

void PlayerView::Render(int xPos, int yPos)
{
    m_pPlayer.Render(m_srcX, m_srcY, xPos, yPos);
}

void PlayerView::Update()
{
    m_pCollider->SetRect(m_pPlayer.GetDstRect());
}

void PlayerView::SetAnimationImage(Constants::MovementDirections facing)
{
    // Selects the image based on the current animation
    m_srcX = (m_currentFrame + Constants::k_playerSpriteSheetPos) * Constants::k_floorSize;
    // Selects the animation timeline based on the direction the character is facing
    m_srcY = facing * Constants::k_floorSize;
}

// This rotates between the 3 different animations for the direction the player is moving
void PlayerView::SetAnimationFrame()
{
    (m_currentFrame != m_timelineLength - 1) ? m_currentFrame += 1 : m_currentFrame = 0;
}