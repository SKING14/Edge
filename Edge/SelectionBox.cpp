#include "SelectionBox.h"

#include <fstream>
#include <cassert>

#include "Constants.h"
#include "TextFileReader.h"

SelectionBox::SelectionBox(const char *pFilename, SDL_Renderer* pRen)
    : TextBox(pFilename, pRen)
{
    SelectionInit(pFilename);
}

void SelectionBox::SelectionInit(const char *pFilename)
{
    std::string entireText;
    std::string variable;

    TextFileReader::ReadEntireTextToString(pFilename, entireText);
    TextFileReader::GetColorFromString(entireText, m_SelectionColor, (std::string)"Selection Color: ");

    const char *pText = m_pTextFile.c_str();
    TextFileReader::ReadTextForMenu(pText, m_pSelections);
    m_pSelection = m_pSelections[m_selectionNumber].first;
}

SelectionBox::~SelectionBox()
{
    for (unsigned int i{ 0 }; i < m_pSelections.size(); ++i)
    {
        delete m_pSelections[i].first;
        m_pSelections[i].first = nullptr;
    }
}

// Each selection text is contained by a pair of chars (the name of the selection, which is rendered)
//  and an int (the number of letters in the name).  
//    Each selection text's length is rendered based on how many letters it contains.
void SelectionBox::Render()
{
    SDL_Color color;

    // For every choice in the selection box
    for (unsigned int i{ 0 }; i < m_pSelections.size(); ++i)
    {
        // If the user is on this choice highlight it
        (m_pSelection == m_pSelections[i].first) ? (color = m_SelectionColor) : (color = m_textColor);

        // Create Line
        SDL_Surface* textSurface = TTF_RenderText_Solid(m_pFont, m_pSelections[i].first, color);
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(m_pRen, textSurface);

        // Setup Text Rect
        SDL_Rect textRect{ 
            static_cast<int>(HALF(Constants::k_winWidth) - HALF(m_pSelections[i].second * m_fontSize)),
            static_cast<int>(TWOTHIRD(Constants::k_winHeight) + (i * m_fontSize)),
            m_pSelections[i].second * m_fontSize,
            m_fontSize };

        // Copy Line
        SDL_RenderCopy(m_pRen, textTexture, nullptr, &textRect);
        SDL_FreeSurface(textSurface);
    };
}

// Moves the user's cursor up and highlights the option
void SelectionBox::MoveUp()
{
    if (m_selectionNumber - 1 >= 0)
    {
        --m_selectionNumber;
        m_pSelection = m_pSelections[m_selectionNumber].first;
    }
}

// Moves the user's cursor down and highlights the option
void SelectionBox::MoveDown()
{
    if (m_selectionNumber < static_cast<int>(m_pSelections.size() - 1))
    {
        ++m_selectionNumber;
        m_pSelection = m_pSelections[m_selectionNumber].first;
    }
}

// Returns the selected option
int SelectionBox::SelectionMade()
{
    return m_selectionNumber;
}