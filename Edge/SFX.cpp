#include "SFX.h"

#include <cassert>

SFX::SFX(char* filename)
{
    m_sfx = Mix_LoadWAV(filename);
    assert(m_sfx != nullptr);
}

// To play the sound 'x' amount of times, change the last parameter. (0 = 1x)
void SFX::Play()
{
    Mix_Resume(2);
    Mix_PlayChannel(2, m_sfx, 0);
}