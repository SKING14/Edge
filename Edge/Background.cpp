#include "Background.h"

Background::Background(SDL_Renderer* pRen)
    : m_pRen (pRen)
{
    m_pBG = IMG_LoadTexture(m_pRen, m_pBGFilename);
}

Background::~Background()
{
}

void Background::Render()
{
    SDL_RenderCopy(m_pRen, m_pBG, &m_src, &m_dst);
}