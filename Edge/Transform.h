#pragma once

#include <vector>
#include <glm/mat4x4.hpp>


class Transform
{
private:
    Transform *m_pParent;
    std::vector<Transform*> m_children;
    glm::mat4 m_modelMatrix;

public:
    Transform() {}
    ~Transform() {}

    glm::mat4 GetModelMatrix();

    void Addchild(Transform *pChild) { m_children.push_back(pChild); }
    void SetParent(Transform *pParent) { m_pParent = pParent; } 
    
};

