#pragma once
#include "TextBox.h"

#include <utility>
#include <vector>
#include <SDL_ttf.h>

// A selection box is a text box with choices for the user to select from
class SelectionBox : public TextBox
{
private:
    typedef std::pair<char*, short int> Selection;
    typedef std::vector<Selection> Selections;

    SDL_Color m_SelectionColor;
    Selections m_pSelections;
    char* m_pSelection{ nullptr };
    int m_selectionNumber{ 0 };

public:
    SelectionBox(const char *pFilename, SDL_Renderer* pRen);
    ~SelectionBox();
    void Render();
    void MoveUp();
    void MoveDown();
    int SelectionMade();

private:
    void SelectionInit(const char *pFilename);
};