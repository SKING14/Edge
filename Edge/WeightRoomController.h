#pragma once
#include "RoomController.h"

#include "DoorController.h"

class HallwayEvent;
class ButtonEvent;

class WeightRoomController : public RoomController
{
private:
    HallwayEvent *m_pEnterRecRoomEvent;
    HallwayEvent *m_pEnterRightSkyRoomEvent;

    ButtonEvent *m_pRecRoomButtonEvent;
    ButtonEvent *m_pRightSkyRoomButtonEvent;

    DoorController m_recDoorController;
    DoorController m_rightSkyDoorController;

public:
    WeightRoomController(EventSystem * pEventSystem, SDL_Renderer * pRen, PlayerController * pPlayerController, MasterSystem * pMasterSystem);
    ~WeightRoomController();

    void GenerateTriggers();

    void Init() override;
    void CleanupEvents() override;
    void CheckDialogueBoxes() override;
    void RenderUniqueRoomObjects() override;
    void AddEvents() override;
    void AddListeners() override;
    void TriggerProceedEvents() override;
};