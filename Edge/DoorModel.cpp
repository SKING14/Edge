#include "DoorModel.h"

#include "CollisionShapeAABB.h"
#include "TextFileReader.h"

DoorModel::DoorModel(const char* pFilename, int ID)
	: m_ID(ID)
{
    std::string entireText;
    SDL_Rect dimensions;
    TextFileReader::ReadEntireTextToString(pFilename, entireText);
    TextFileReader::GetRecFromString(entireText, dimensions, (std::string)"Destination: ");
    
    m_pCollider = new CollisionShapeAABB(dimensions);
}

DoorModel::~DoorModel()
{
    delete m_pCollider;
    m_pCollider = nullptr;
}
