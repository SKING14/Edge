#pragma once

#include "AudioSystem.h"
#include "HallwaySystem.h"
#include "PlayerSystem.h"
#include "DoorSystem.h"

class MasterSystem
{
private:
    AudioSystem m_audioSystem;
    HallwaySystem m_hallwaySystem;
    PlayerSystem m_playerSystem;
    DoorSystem m_doorSystem;

public:
    MasterSystem(PlayerController *pController);
    ~MasterSystem() {}

    void AddRoom(RoomController *pRoom);
    void AddDoor(DoorModel *pDoor);

    EventListener* GetAudioSystem(){return &m_audioSystem;}
    EventListener* GetHallwaySystem(){return &m_hallwaySystem;}
    EventListener* GetPlayerSystem(){return &m_playerSystem;}
    EventListener* GetDoorSystem(){return &m_doorSystem;}
};

