#pragma once
#include "EventListener.h"

class DialogueBox;

class DialogueBoxSystem : public EventListener
{
private:
    DialogueBox *m_pDialogueBox{ nullptr };

public:
    DialogueBoxSystem(DialogueBox *pDialogueBox) { m_pDialogueBox = pDialogueBox; }
    ~DialogueBoxSystem() {}

    void HandleEvent(const Event * pEvent) override;
};