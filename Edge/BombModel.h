#pragma once
#include "Model.h"

#include <SDL.h>

class BombModel : public Model
{
private:
    bool m_active{ false };

public:
    BombModel() {}
    BombModel(SDL_Rect dst);
    ~BombModel();
    void SetInactive();
};

