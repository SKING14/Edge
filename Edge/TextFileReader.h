#pragma once

#include <vector>
#include <SDL.h>
#include <string>

class Image;
class Wall;

class TextFileReader
{
private:
    typedef std::pair<char*, short int> Selection;
    typedef std::vector<Selection> Selections;
    typedef std::pair<const char*, short int> SingleLine;
    typedef std::vector<SingleLine> DialogueLines;

public:
    TextFileReader() {}
    ~TextFileReader() {}

    static void ReadTextForMenu(const char* pFilename, Selections& container);
    static void ReadEntireTextToString(const char* pTextFilename, std::string& completeTextContainer );
    static void ReadWallsTypeAndPositionForRoom(const char* pFilename, std::vector<std::string>& walls);
    static void GetDialogueFromText(const char * pFilename, DialogueLines& lines, int charsPerLine);
    static std::string GetSelectedDataFromString(std::string& entireString, std::string name);
    static void GetRecFromString(std::string & variableText, SDL_Rect & rect, std::string & variableName);
    static size_t GetValueFromString(std::string & string, size_t start, int& variable);
    static void GetColorFromString(std::string & variableText, SDL_Color & color, std::string & variableName);
    static size_t GetColorValueFromString(std::string & string, size_t offset, Uint8 &variable);
};