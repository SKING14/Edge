#include "TextFileReader.h"

#include <fstream>
#include <cassert>
#include <string>
#include <iostream>

#include "Constants.h"

// This reads in the different selections for a menu
void TextFileReader::ReadTextForMenu(const char* pFilename, Selections& container)
{
    std::ifstream ifs;
    ifs.open(pFilename, std::ifstream::in);
    assert(ifs.is_open());

    char letterReceived{};

    while (!ifs.eof())
    {
        Selection selection;
        short int lineLength{ 0 };
        char* textLine = new char[Constants::k_MaxSelectBoxLineLength];


        while ((letterReceived = ifs.get()) != '\n' && !ifs.eof())
        {
            textLine[lineLength] = letterReceived;
            ++lineLength;
        }

        textLine[lineLength] = 0;

        selection = std::make_pair(textLine, lineLength);

        container.push_back(selection);
    }

    ifs.close();
}

// This reads an entire text into a string
void TextFileReader::ReadEntireTextToString(const char* pTextFilename, std::string& completeTextContainer)
{
    std::string myString{};
    std::ifstream ifs;

    ifs.open(pTextFilename, std::ifstream::in);

    assert(ifs.is_open());

    while (!ifs.eof())
    {
        completeTextContainer += ifs.get();
    }

    ifs.close();
}

// This reads in the different types of walls and their position for a room
void TextFileReader::ReadWallsTypeAndPositionForRoom(const char* pFilename, std::vector<std::string>& walls)
{
    std::ifstream ifs;

    ifs.open(pFilename, std::ifstream::in);

    assert(ifs.is_open());
    
    while (!ifs.eof())
    {
        std::string wallType;
        char letter{};
		
        while ((letter = ifs.get()) != ',' && letter != '\n' && !ifs.eof())
        {
            if (letter != ' ')
            {
                wallType += letter;
            }
        }
        walls.push_back(wallType);
    }

    ifs.close();
}

void TextFileReader::GetDialogueFromText(const char* pFilename, DialogueLines& lines, int charsPerLine)
{
    std::ifstream ifs;
    ifs.open(pFilename, std::ifstream::in);
    assert(ifs.is_open());

    while (!ifs.eof())
    {
        char* textLine = new char[charsPerLine];
        int lineLength{ charsPerLine };

        // Read 26 characters (the maximum number of characters on a single line) from the .txt file
        ifs.read(textLine, charsPerLine);
        if (ifs.eof())
        {
            lineLength = (int)ifs.gcount();
            textLine[lineLength] = 0;
        }
        else
        {
            // These if/else statements makes sure that the line does not end in the middle of a word
            textLine[ifs.gcount()] = 0;
            if (textLine[charsPerLine] != ' ' && !ifs.eof())
            {
                char nextChar;
                ifs.get(nextChar);

                if (nextChar != ' ' && !ifs.eof())
                {
                    ifs.putback(nextChar);

                    int i{ 1 };
                    while (textLine[charsPerLine - i] != ' ')
                    {
                        ifs.putback(textLine[charsPerLine - i]);
                        ++i;
                    }
                    textLine[charsPerLine - i] = 0;
                    lineLength = charsPerLine - i;
                }
                else
                {
                    textLine[charsPerLine + 1] = 0;
                }
            }
            else
            {
                textLine[charsPerLine + 1] = 0;
            }
        }

        SingleLine line = std::make_pair(textLine, lineLength);
        lines.push_back(line);
    }
    ifs.close();
}

// This will get the set of specific data requested (i.e. "Source: ", "Image: ", etc.)
std::string TextFileReader::GetSelectedDataFromString(std::string& entireString, std::string name)
{
    size_t variableStart;
    size_t subStringStart;
    size_t subStringEnd;
    std::string newString;

    variableStart = entireString.find(name);
    subStringStart = entireString.find('[', variableStart);
    subStringEnd = entireString.find(']', variableStart);
    return entireString.substr(subStringStart + 1, ((subStringEnd - subStringStart) - 1));
}

// This will get the x, y, w, and h for a rec from a string
void TextFileReader::GetRecFromString(std::string &variableText, SDL_Rect &rect, std::string &variableName)
{
    size_t offset;
    std::string variable;
    // Set src rect
    variable = TextFileReader::GetSelectedDataFromString(variableText, variableName);
    offset = TextFileReader::GetValueFromString(variable, 0, rect.x);
    offset = TextFileReader::GetValueFromString(variable, offset, rect.y);
    offset = TextFileReader::GetValueFromString(variable, offset, rect.w);
    TextFileReader::GetValueFromString(variable, offset, rect.h);
}

size_t TextFileReader::GetValueFromString(std::string& string, size_t offset, int& variable)
{
    std::string subString;
    const char* valueFromString;
    size_t end;

    end = string.find_first_of(",]", offset);
    subString = string.substr(offset, end);
    valueFromString = subString.c_str();
    variable = atoi(valueFromString);
    return end + 1;
}

// SDL_Color stores rgba as Uint8, it also doesn't allow me to use indirection
//  so I have to create almost identical functions to GetRecFromString
void TextFileReader::GetColorFromString(std::string &variableText, SDL_Color &color, 
    std::string &variableName)
{
    size_t offset;
    std::string variable;
    // Set src rect
    variable = TextFileReader::GetSelectedDataFromString(variableText, variableName);
    offset = TextFileReader::GetColorValueFromString(variable, 0, color.r);
    offset = TextFileReader::GetColorValueFromString(variable, offset, color.g);
    offset = TextFileReader::GetColorValueFromString(variable, offset, color.b);
    TextFileReader::GetColorValueFromString(variable, offset, color.a);
}

size_t TextFileReader::GetColorValueFromString(std::string& string, size_t offset, Uint8 &variable)
{
    std::string subString;
    const char* valueFromString;
    size_t end;

    int converter;

    end = string.find_first_of(",]", offset);
    subString = string.substr(offset, end);
    valueFromString = subString.c_str();
    converter = atoi(valueFromString);
    variable = (Uint8)converter;
    return end + 1;
}