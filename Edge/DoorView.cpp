#include "DoorView.h"


DoorView::DoorView(const char *pFilename, SDL_Renderer *pRen, SDL_Rect dimensions)
    : View(pFilename, pRen, dimensions)
{
}

void DoorView::Update(bool open)
{
    if (open != m_previousState)
    {
        (open) ? OpenDoor() : CloseDoor();
        m_previousState = !m_previousState;
    }
}

void DoorView::OpenDoor()
{
    // time is in milliseconds
    m_myTimer = SDL_GetTicks();
    m_openening = true;
}

void DoorView::CloseDoor()
{
    m_myTimer = SDL_GetTicks();
    m_openening = false;
}