#include "Image.h"

#include <cassert>

#include "Constants.h"
#include "TextFileReader.h"

//--------------------------------------------------------------------------------
// Constructors
//--------------------------------------------------------------------------------


Image::Image(const char* pTextFilename, SDL_Renderer* pRen)
    : m_pRen(pRen)
{
    std::string entireText;
    LoadImage(pTextFilename, entireText);
    TextFileReader::GetRecFromString(entireText, m_src, (std::string)"Source: ");
    TextFileReader::GetRecFromString(entireText, m_dst, (std::string)"Destination: ");
}

Image::Image(const char* pTextFilename, SDL_Renderer* pRen, SDL_Rect dst)
    : m_pRen(pRen)
{
    std::string entireText;
    LoadImage(pTextFilename, entireText);
    TextFileReader::GetRecFromString(entireText, m_src, (std::string)"Source: ");
    m_dst = dst;
}

Image::Image(SDL_Texture *pTexture, SDL_Renderer *pRen, SDL_Rect src, SDL_Rect dst)
    : m_pRen(pRen)
    , m_pTexture(pTexture)
{
    m_src = src;
    m_dst = dst;
}

//--------------------------------------------------------------------------------
// Methods
//--------------------------------------------------------------------------------

void Image::LoadImage(const char* pTextFilename, std::string& entireText)
{
    TextFileReader::ReadEntireTextToString(pTextFilename, entireText);

    std::string newString;

    // Load texture with Image
    newString = TextFileReader::GetSelectedDataFromString(entireText, "Image: ");
    m_pImageFilename += newString;
    const char *cstr = m_pImageFilename.c_str();
    m_pTexture = IMG_LoadTexture(m_pRen, cstr);
    assert(m_pTexture != nullptr);
}

// Fills the entire screen(window) with image (should be used for floor of rooms)
void Image::FillScreen()
{
    int y{ 0 };
    while (y < Constants::k_winHeight)
    {
        m_dst.y = y;
        int x{ 0 };
        while (x < Constants::k_winWidth)
        {
            m_dst.x = x;
            SDL_RenderCopy(m_pRen, m_pTexture, &m_src, &m_dst);
            x += Constants::k_floorSize;
        }
        y += Constants::k_floorSize;
    }
}

//--------------------------------------------------------------------------------
// Renders
//--------------------------------------------------------------------------------

void Image::Render()
{
    SDL_RenderCopy(m_pRen, m_pTexture, &m_src, &m_dst);
}

// Function overload which allows the caller to set the x and y for the src and dst, without using the current x and y
void Image::Render(int srcX, int srcY, int dstX, int dstY)
{
    m_src.x = srcX;
    m_src.y = srcY;
    m_dst.x = dstX;
    m_dst.y = dstY;
    
    SDL_RenderCopy(m_pRen, m_pTexture, &m_src, &m_dst);
}