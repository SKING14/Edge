#pragma once
#include "EventListener.h"

#include <functional>

#include "Constants.h"
#include "SFX.h"
#include "BgMusic.h"

class AudioSystem : public EventListener
{
private:
    SFX m_scroll{ SOUNDS("SFXScroll.wav") };
    SFX m_select{ SOUNDS("SFXSelect.wav") };
    SFX m_turnPage{ SOUNDS("SFXTurnPage.wav") };
    SFX m_openDoor{ SOUNDS("SFXOpenDoor.wav") };
    SFX m_explosion{ SOUNDS("SFXExplosion.wav") };
    SFX m_fuseBurning{ SOUNDS("SFXFuseBurning.wav") };
    BgMusic m_bgMusicMellow{ SOUNDS("PirateBooty.wav") };

public:
    AudioSystem() {}
    ~AudioSystem() {}

    void HandleEvent(const Event* pEvent) override;
};