#pragma once

#define HALF(x) (x / 2)
#define TWOTHIRD(x) (x * 2/3)
#define VARIABLES(x) "Assets/Variables/" x ".txt"
#define IMAGES(x) "Assets/Images/" x
#define SOUNDS(x) "Assets/Sounds/" x
#define TEXTS(x) "Assets/Texts/" x ".txt"
#define MOVE(x) Constants::MovementDirections::x
#define PLAYERACTION(x) Constants::PlayerActions::x

struct Constants
{
    static const int k_winX{ 100 };
    static const int k_winY{ 100 };
    static const int k_winWidth{ 1024 };
    static const int k_winHeight{ 768 };
    static const int k_floorSize{ 32 };
    static const int k_doubleFloorSize{ 64 };
    static const int k_playerSpriteSheetPos{ 6 };
    static const short int k_MaxSelectBoxLineLength{ 20 };
    static const int k_maxCharsPerLine{ 26 };

    enum CursorDirections
    {
        ScrollUp,
        ScrollDown,
        Select
    };

    enum PlayerActions
    {
        Proceed,
        Interact,
        Bomb,
        NoAction,
        Quit
    };

    // I always list directions "Up, Down, Left, Right", the sprite sheet has them printed a different way,
    // so in order to stay consistant they are "out of order"
    enum MovementDirections
    {
        Up = 3,
        Down = 0,
        Left = 1,
        Right = 2,
        Idle = 4
    };
};