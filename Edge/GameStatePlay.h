#pragma once
#include "GameState.h"

#include "RoomController.h"
#include "MasterSystem.h"
#include "PlayerController.h"

class EventSystem;

class GameStatePlay : public GameState
{
private:
	EventSystem *m_pEventSystem{ nullptr };
    SDL_Renderer *m_pRen{ nullptr };
    RoomController *m_pCurrentRoom{ nullptr };

    MasterSystem m_masterSystem;
    PlayerController m_playerController;
    RoomController::RoomName m_newRoom{ ROOM(PlayersRoom) };
    RoomController::RoomName m_currentRoomName{ ROOM(PlayersRoom) };

public:
    GameStatePlay(EventSystem *pEventSystem, SDL_Renderer *pRen);
    ~GameStatePlay();

    void Enter(GameStateMachine *gsm) override;
    bool Update() override;
    void Exit() override;

private:
    void SetNewRoom();

};