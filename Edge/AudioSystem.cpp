#include "AudioSystem.h"

#include "Event.h"

void AudioSystem::HandleEvent(const Event* pEvent)
{
    switch (pEvent->GetID())
    {
    case EventIDManager::UpKeyPressedEventID:
        m_scroll.Play();
        break;
    case EventIDManager::DownKeyPressedEventID:
        m_scroll.Play();
        break;
    case EventIDManager::SelectionMadeEventID:
        m_select.Play();
        break;
    case EventIDManager::ExitDialogueBoxEventID:
        m_bgMusicMellow.Play();
        break;
    case EventIDManager::DoorOpenEventID:
        m_openDoor.Play();
        break;
    case EventIDManager::PlayerSetBombEventID:
        m_fuseBurning.Play();
        break;
    default:
        break;
    }
}

