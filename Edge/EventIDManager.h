#pragma once

#define ID(x) EventIDManager::x

struct EventIDManager
{
public:
    enum EventID
    {
        UpKeyPressedEventID,
        DownKeyPressedEventID,
        SelectionMadeEventID,
        IntroTriggerEventID,
        PlayerProceedEventID,
        EnterRecFromPlayersEventID,
        EnterPlayersFromRecEventID,
        EnterWeightFromRecEventID,
        PlayersToRecButtonEventID,
        ExitDialogueBoxEventID,
        RecToPlayerButtonEventID,
        RecToWeightButtonEventID,
        PlayerSetBombEventID,
        WeightToRecButtonEventID,
        WeightToRightSkyButtonEventID,
        EnterRecFromWeightEventID,
        EnterRightSkyFromWeightEventID,
        DoorOpenEventID,
        SkyToRightWeightButtonEventID,
        EnterRightWeightFromSkyEventID,
        GameOverEventID
    };
};