#include "GLRenderer.h"

#include <string>
#include <fstream>
#include <vector>

#include <glm\gtx\transform.hpp>

void GLRenderer::LoadShaders(const char *vertexShaderPath,
	const char *fragmentShaderPath)
{
	// Create the shader IDs
	VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read shader files
	std::string VertShaderCode = GetShaderCode(vertexShaderPath);
	std::string FragShaderCode = GetShaderCode(fragmentShaderPath);

	// Compile
	CompileShader(VertexShaderID, vertexShaderPath, VertShaderCode);
	CompileShader(FragmentShaderID, fragmentShaderPath, FragShaderCode);

	// Link
	ShaderProgramID = glCreateProgram();
	glAttachShader(ShaderProgramID, VertexShaderID);
	glAttachShader(ShaderProgramID, FragmentShaderID);
	glLinkProgram(ShaderProgramID);

	// Check for errors
	GLint Result = GL_FALSE;
	GLint InfoLogLength;

	glGetProgramiv(ShaderProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ShaderProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> ErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ShaderProgramID, InfoLogLength, NULL, &ErrorMessage[0]);
	}
}

void GLRenderer::CompileShader(GLuint shaderID, const char * path, std::string code)
{
	GLint Result = GL_FALSE;
	int InfoLogLength;

	const char *source = code.c_str();
	glShaderSource(shaderID, 1, &source, NULL);
	glCompileShader(shaderID);

	// Check for errors!
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0)
	{
		std::vector<char> ErrorMessage(InfoLogLength + 1);

		char* error = new char[InfoLogLength + 1];
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, error);
		delete[] error;
	}
}

std::string GLRenderer::GetShaderCode(const char * path)
{
	// Read from file
	std::string Code;
	std::ifstream InFile(path, std::ios::in);
	if (InFile.is_open())
	{
		std::string Line = "";
		while (getline(InFile, Line))
		{
			Code += '\n' + Line;
		}

		InFile.close();
	}

	return Code;
}

bool GLRenderer::Init(SDL_Window *window)
{
	this->window = window;

	// Create the context for OpenGL
	MainContext = SDL_GL_CreateContext(window);

	// Initialize GLEW
	glewExperimental = true;
	glewInit();

	LoadShaders("simple.vert", "simple.frag");

	// Get the location(ID) of the uniform inside of our shader
	MVPUniformID = glGetUniformLocation(ShaderProgramID, "MVP");

	// Target OpenGL 3.3
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_DEPTH_TEST);

#if 0
	// Create the vertex array and get the ID
	glGenVertexArrays(1, &VertexArrayObjectID);

	// Bind the vertex array (Consquent operations will applied to the bound object)
	glBindVertexArray(VertexArrayObjectID);

	// Create the vertex buffer and get the ID
	glGenBuffers(1, &VertexBufferObjectID);

	// Bind it!
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferObjectID);

	// Give our verts to the VBO
	//LOG_TRACE_DATA("Size: %d", sizeof(verts));
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

	// Generate Index Buffer
	glGenBuffers(1, &IndexBufferObjectID);

	// Bind it as an IBO (Index Buffer Object)
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferObjectID);

	// TODO: Make Indices
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices)
		* sizeof(GLushort), indices, GL_STATIC_DRAW);

	// Setting the attribute pointer for position
	glVertexAttribPointer(
		0,						// Attribute Index (come into play during shaders)
		3,						// size
		GL_FLOAT,				// type
		GL_FALSE,				// normalized?
		sizeof(Vertex),			// stride (size of the vertex object)
		(void*)0				// array buffer offset
	);

	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)12
	);
#endif
	objLoader.CreateMesh("Assets/Objects/TNT.obj");
	// Using our mesh class!
	//m1.Init(verts, sizeof(verts) / sizeof(verts[0]),
	//	indices, sizeof(indices) / sizeof(indices[0]));

	//verts[0].position.y += 2;

	//m2.Init(verts, sizeof(verts) / sizeof(verts[0]),
	//	indices, sizeof(indices) / sizeof(indices[0]));

	//m1.m_modelMatrix = glm::translate(glm::vec3(-3, 0, 0));
	//m2.m_modelMatrix = glm::translate(glm::vec3(3, 0, 0));


	//m1.m_modelMatrix = glm::scale(m1.m_modelMatrix, glm::vec3(0.2, 5, 2));

	//m_meshes.push_back(&m1);
	//m_meshes.push_back(&m2);

	// Set up our Matrices
	ModelMatrix = glm::mat4(1);  // Identity
	ViewMatrix = glm::lookAt(
		glm::vec3(0, 4, 10),
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0)
	);

	ProjectionMatrix = glm::perspective(45.0f,
		/* Window ratio */float(800) / 600, 0.1f, 100.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Fill in the pixels between the verts
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Only draw the polygon lines
	//glPolygonMode(GL_FRONT_AND_BACK, GL_POINT); // Just draw the verts

	//Mesh *m = objLoader.Load("Assets/Earth/earth.obj");
	Mesh *m = objLoader.CreateMesh("Assets/Objects/TNT.obj");//objLoader.Load("Assets/Earth/earth.obj");
	m->Init();

	m_meshes.push_back(m);

	return true;
}

void GLRenderer::Draw()
{
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	if (camMoveForward)
	{
		// Translating the view Matrix will move it relative to the View Matrix
		ViewMatrix = glm::translate(ViewMatrix, glm::vec3(0, 0, 0.1f));
	}

	m_meshes[0]->m_modelMatrix = glm::rotate(m_meshes[0]->m_modelMatrix,
		glm::radians(1.0f), glm::vec3(1, 1, 0));

	//static float fakeTime = 0;
	//glBindVertexArray(m2.m_vaoId);
	//fakeTime += 0.1f;
	//m2.m_vertices[0].position.y += sinf(fakeTime);
	//glBindBuffer(GL_ARRAY_BUFFER, m2.vbo);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m2.m_vertices.size(),
	//	&m2.m_vertices[0], GL_DYNAMIC_DRAW);


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use the shader we built!
	glUseProgram(ShaderProgramID);

	//ModelMatrix = glm::rotate(ModelMatrix, 0.025f, glm::vec3(1, 1, 0));

	//for (auto mesh : m_meshes)
	//{
	//	glBindVertexArray(mesh->m_vaoId);
	//	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * mesh->m_modelMatrix;
	//	glUniformMatrix4fv(MVPUniformID, 1, GL_FALSE, &MVP[0][0]);
	//	glDrawArrays(GL_TRIANGLES, 0, mesh->m_vertices.size());
	//}

	glBindVertexArray(m_meshes[0]->m_vaoId);

	for (unsigned int i = 0; i < m_meshes[0]->m_subMeshes.size(); ++i)
	{
		//if (i == 20) continue;

		SubMesh *test = m_meshes[0]->m_subMeshes[i];
		glBindTexture(GL_TEXTURE_2D, test->material->textureID);

		glm::mat4 MVP = ProjectionMatrix * ViewMatrix *
			m_meshes[0]->m_modelMatrix;
		glUniformMatrix4fv(MVPUniformID, 1, GL_FALSE, &MVP[0][0]);
		//glDrawElements(GL_TRIANGLES, test->size, GL_UNSIGNED_SHORT,
		//	(GLvoid*)*(m_meshes[0]->m_indices.begin() + test->offset));
		glDrawArrays(GL_TRIANGLES, test->offset, test->size);
	}
	

	glBindVertexArray(0);

	SDL_GL_SwapWindow(window);
}

void GLRenderer::Cleanup()
{
	glDetachShader(ShaderProgramID, VertexShaderID);
	glDetachShader(ShaderProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	SDL_GL_DeleteContext(MainContext);

	for (auto mesh : m_meshes)
	{
		delete mesh;
	}
}