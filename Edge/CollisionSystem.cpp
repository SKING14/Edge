#include "CollisionSystem.h"

#include "CollisionShapeAABB.h"
#include "CollisionShapePoint.h"
#include "CollisionShapeLine.h"

// This takes in any two collider shapes and calls the appropriate collision detection between those types of shapes
bool CollisionSystem::AreOverlapping(CollisionShape *a, CollisionShape *b)
{
    
    // AABB vs AABB check
    if (a->m_type == CollisionShape::Type::AABB)
    {
        CollisionShapeAABB *colliderA = dynamic_cast<CollisionShapeAABB*>(a);

        if (b->m_type == CollisionShape::Type::AABB)
        {
            CollisionShapeAABB *colliderB = dynamic_cast<CollisionShapeAABB*>(b);
            return AABBOverlapAABB(colliderA, colliderB);
        }
    }
    // Point vs AABB check
    else if (a->m_type == CollisionShape::Type::Point)
    {
        CollisionShapePoint* colliderA = dynamic_cast<CollisionShapePoint*>(a);

        if (b->m_type == CollisionShape::Type::AABB)
        {
            CollisionShapeAABB *colliderB = dynamic_cast<CollisionShapeAABB*>(b);
            return PointOverAABB(colliderA, colliderB);
        }
    }
    // Line vs AABB check
    else if (a->m_type == CollisionShape::Type::Line)
    {
        CollisionShapeLine* colliderA = dynamic_cast<CollisionShapeLine*>(a);

        if (b->m_type == CollisionShape::Type::AABB)
        {
            CollisionShapeAABB *colliderB = dynamic_cast<CollisionShapeAABB*>(b);
            return LineOverAABB(colliderA, colliderB);
        }
    }
    return false;
}

bool CollisionSystem::AABBOverlapAABB(CollisionShapeAABB* pA, CollisionShapeAABB* pB)
{
    return (pA->GetX() < pB->GetX() + pB->GetW() &&
        pA->GetX() + pA->GetW() > pB->GetX() &&
        pA->GetY() < pB->GetY() + pB->GetH() &&
        pA->GetH() + pA->GetY() > pB->GetY());
}

bool CollisionSystem::PointOverAABB(CollisionShapePoint* point, CollisionShapeAABB* AABB)
{
    return (point->GetX() >= AABB->GetX()) &&
        (point->GetX() <= AABB->GetX() + AABB->GetW()) &&
        (point->GetY() >= AABB->GetY()) &&
        (point->GetY() <= AABB->GetY() + AABB->GetH());
}

bool CollisionSystem::LineOverAABB(CollisionShapeLine* line, CollisionShapeAABB* AABB)
{
    // This checks if the start point of the line is intersecting with AABB
    if ((line->GetStartX() >= AABB->GetX()) &&
        (line->GetStartX() <= AABB->GetX() + AABB->GetW()) &&
        (line->GetStartY() >= AABB->GetY()) &&
        (line->GetStartY() <= AABB->GetY() + AABB->GetH()))
    {
        return true;
    }
    // This checks if the end point of the line is intersecting with AABB
    else if ((line->GetEndX() >= AABB->GetX()) &&
        (line->GetEndX() <= AABB->GetX() + AABB->GetW()) &&
        (line->GetEndY() >= AABB->GetY()) &&
        (line->GetEndY() <= AABB->GetY() + AABB->GetH()))
    {
        return true;
    }
    else
    {
        return false;
    }
}