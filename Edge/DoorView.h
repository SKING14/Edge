#pragma once
#include "View.h"

#include <SDL.h>
#include <vector>
#include <limits>

class DoorView : public View
{
private:
    bool m_openening{ false };
    bool m_previousState{ false };

public:
    DoorView(const char *pFilename, SDL_Renderer *pRen, SDL_Rect dimensions);
    ~DoorView() {}

    void Update(bool open);

private:
    void OpenDoor();
    void CloseDoor();

};