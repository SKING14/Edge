#pragma once

#include <GL\glew.h>
#include <glm\mat4x4.hpp>
#include <glm\vec3.hpp>
#include <SDL.h>
#include <string>
#include <vector>

/*

Assimp::Importer importer;

const aiScene *scene = importer.ReadFile(
"Assets/Earth/earth.obj", aiProcess_Triangulate);

*/

#include "Mesh.h"
#include "ObjLoader.h"

class GLRenderer
{
private:
    GLuint VertexArrayObjectID;
    GLuint VertexBufferObjectID;
    GLuint IndexBufferObjectID;

    GLuint VertexShaderID;
    GLuint FragmentShaderID;
    GLuint ShaderProgramID;

    GLuint MVPUniformID;

    SDL_GLContext MainContext;
    SDL_Window *window;

    static Vertex verts[8];
    static GLushort indices[36];

    Mesh m1, m2;
    std::vector<Mesh*> m_meshes;

    ObjLoader objLoader;

    // Stepping into the world of 3D
    glm::mat4 ModelMatrix;
    glm::mat4 ViewMatrix;
    glm::mat4 ProjectionMatrix;

    void LoadShaders(const char *vertexShaderPath, const char *fragmentShaderPath);
    void CompileShader(GLuint shaderID, const char *path, std::string code);
    std::string GetShaderCode(const char *path);

public:
    GLRenderer() {}
    ~GLRenderer() {}

    bool Init(SDL_Window *window);
    void Draw();
    void Cleanup();

    bool camMoveForward{ false };
};

