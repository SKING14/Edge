#pragma once
#include "Audio.h"

#include <SDL_mixer.h>

class SFX : public Audio
{
private:
    Mix_Chunk *m_sfx;
public:
    SFX(char* filename);
    void Play();
    ~SFX() {}
};

