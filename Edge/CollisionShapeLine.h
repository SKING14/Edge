#pragma once
#include "CollisionShape.h"

class CollisionShapeLine : public CollisionShape
{
private:
    SDL_Point m_startPoint;
    SDL_Point m_endPoint;
public:
    CollisionShapeLine(int startX, int startY, int endX, int endY) 
        {    SetPoints(startX, startY, endX, endY);  m_type = Type::Line; }
    ~CollisionShapeLine() {}

    // Mutators
    void SetPoints(int startX, int startY, int endX, int endY)
    {
        m_startPoint = { startX, startY };
        m_endPoint = { endX, endY };   
    }

    // Accessors
    int GetStartX() const { return m_startPoint.x; }
    int GetStartY() const { return m_startPoint.y; }
    int GetEndX() const { return m_endPoint.x; }
    int GetEndY() const { return m_endPoint.y; }
};

