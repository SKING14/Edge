#include "GameStateGL.h"

#include "GameStateMachine.h"
#include <conio.h>
#include <GL\glew.h>

GameStateGL::GameStateGL()
{

}

GameStateGL::~GameStateGL()
{

}

void GameStateGL::Enter(GameStateMachine *gsm)
{
    GameState::Enter(gsm);
}

bool GameStateGL::Update()
{
    _getch();
    m_pStateMachine->ChangeState(GameStateMachine::State::Play);
    return true;
}

void GameStateGL::Exit()
{

}
