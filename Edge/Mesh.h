#pragma once

#include <vector>
#include <map>
#include <string>

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <GL/glew.h>

struct Vertex
{
    glm::vec3 position;
    glm::vec2 uv;
};

struct Material
{
    GLuint textureID;
};

struct SubMesh
{
    Material *material;
    GLuint offset;
    GLuint size;
};

class Mesh
{
public:
    Mesh();
    ~Mesh();

    std::map<std::string, Material*> m_materials;

    GLuint m_vaoId;
    GLuint vbo;

    std::vector<glm::vec2> m_uvs;
    std::vector<Vertex> m_vertices;
    std::vector<GLushort> m_indices;

    // X, Y, Z, X, Y, Z  (for verts until done)
    // U, V, U, V (for uvs until done)
    std::vector<GLfloat> m_vertPieces;

    glm::mat4 m_modelMatrix;

    std::vector<SubMesh*> m_subMeshes;

    void Init();
};

