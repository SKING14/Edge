#pragma once
#include "Model.h"

#include <SDL.h>

class CollisionShapeAABB;

class DoorModel : public Model
{
private:
    bool m_open{ false };
	int m_ID;

public:
    DoorModel(const char* pFilename, int ID);
    ~DoorModel();

    // Mutators
    void Open() { m_open = true; }
    void Close() { m_open = false; }

    // Accessors
    bool CheckIfOpen() const { return m_open; }
	int GetID() const { return m_ID; }

};