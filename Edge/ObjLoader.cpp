#include "ObjLoader.h"

#include <fstream>
#include <string>
#include <cassert>
#include <SDL_image.h>
#include <iostream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define PRE_SPACE(x) x.substr(0, x.find_first_of(' '))
#define POST_SPACE(x) x.substr(x.find_first_of(' ') + 1)

ObjLoader::ObjLoader()
{
    // There can only be one
    static int count = 0;
    ++count;

    assert(count == 1);

    assert(IMG_Init(IMG_INIT_PNG) == IMG_INIT_PNG);
}


ObjLoader::~ObjLoader()
{
    IMG_Quit();
}

Mesh* ObjLoader::Load(const char * filename)
{
    std::ifstream in;

    in.open(filename, std::ios::in);

    assert(in.is_open());

    std::vector<glm::vec3> temp_verts;
    std::vector<glm::vec2> temp_uvs;

    Mesh *mesh = new Mesh();
    bool readingFaces = false;
    std::string nextMaterialName;
    SubMesh *subMesh = nullptr;
    GLuint vertCount = 0;

    while (!in.eof())
    {
        std::string line;
        std::getline(in, line, '\n');

        std::string header = PRE_SPACE(line);

        // Vertex
        if (header == "v")
        {
            glm::vec3 position;

            // sscanf is in the fprintf Family
            sscanf_s(line.c_str(), "v %f %f %f", &position.x, &position.y, &position.z);
            //mesh->m_vertices.push_back(Vertex{ position, glm::vec2(1, 1) });
            temp_verts.push_back(position);
        }
        // Texture
        else if (header == "vt")
        {
            glm::vec2 uv;
            sscanf_s(line.c_str(), "vt %f %f", &uv.x, &uv.y);
            temp_uvs.push_back(uv);
        }
        // Object
        else if (header == "o")
        {
            if (subMesh != nullptr)
            {
                subMesh->size = vertCount - subMesh->offset;
            }

            // New sub-object
            //	Material*
            subMesh = new SubMesh();
            subMesh->material = mesh->m_materials[nextMaterialName];
            subMesh->offset = vertCount;

            mesh->m_subMeshes.push_back(subMesh);
        }
        else if (header == "usemtl")
        {
            nextMaterialName = POST_SPACE(line);
        }
        else if (header == "mtllib")
        {
            std::string filepath = "Assets/Objects/" + POST_SPACE(line);
            LoadMaterial(mesh, filepath.c_str());
        }
        // Face
        else if (header == "f")
        {
            GLushort index1, index2, index3;
            GLushort uv1, uv2, uv3;
            GLushort normal1, normal2, normal3;

            sscanf_s(line.c_str(), "f %hu/%hu/%hu %hu/%hu/%hu %hu/%hu/%hu",
                &index1, &uv1, &normal1,
                &index2, &uv2, &normal2,
                &index3, &uv3, &normal3);

            Vertex testVertices[3] =
            {
                { temp_verts[index1 - 1], temp_uvs[uv1 - 1] },
                { temp_verts[index2 - 1], temp_uvs[uv2 - 1] },
                { temp_verts[index3 - 1], temp_uvs[uv3 - 1] },
            };

            for (int testVertIndex = 0; testVertIndex < 3; ++testVertIndex)
            {
                bool found = false;

                for (unsigned int vertIndex = 0; vertIndex < mesh->m_vertices.size();
                ++vertIndex)
                {
                    bool posMatch = testVertices[testVertIndex].position
                        == mesh->m_vertices[vertIndex].position;
                    bool uvMatch = testVertices[testVertIndex].uv
                        == mesh->m_vertices[vertIndex].uv;

                    if (posMatch && uvMatch)
                    {
                        mesh->m_indices.push_back(vertIndex);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    mesh->m_vertices.push_back(testVertices[testVertIndex]);
                    mesh->m_indices.push_back(mesh->m_vertices.size() - 1);
                }
            }

            vertCount += 3;
        }
    }

    subMesh->size = vertCount - subMesh->offset;

    in.close();

    return mesh;
}

void ObjLoader::LoadMaterial(Mesh *mesh, const char *filename)
{
    std::ifstream in;

    in.open(filename, std::ios::in);

    assert(in.is_open());

    std::string line;

    Material *material = nullptr;

    while (!in.eof())
    {
        std::getline(in, line, '\n');
        std::string header = PRE_SPACE(line);

        if (header == "newmtl")
        {
            material = new Material();
            mesh->m_materials.emplace(POST_SPACE(line), material);
        }
        else if (header == "\tmap_Kd")
        {

            glGenTextures(1, &material->textureID);
            glBindTexture(GL_TEXTURE_2D, material->textureID);

            std::string filepath = "Assets/Objects/" + POST_SPACE(line);
            SDL_Surface *surface = IMG_Load(filepath.c_str());

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, surface->pixels);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        }
    }

    in.close();
}

Mesh* ObjLoader::CreateMesh(const char * filename)
{
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(filename,
        aiProcess_Triangulate);

    Mesh* mesh = new Mesh();

    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        SubMesh *subMesh = new SubMesh();
        aiMesh *curMesh = scene->mMeshes[i];

        unsigned int matIndex = curMesh->mMaterialIndex;
        aiMaterial* material = scene->mMaterials[matIndex];
        aiString matName;
        material->Get(AI_MATKEY_NAME, matName);

        if (mesh->m_materials.find(matName.C_Str()) == mesh->m_materials.end())
        {
            // Loading a new material...
            Material *mat = new Material();
            glGenTextures(1, &mat->textureID);
            glBindTexture(GL_TEXTURE_2D, mat->textureID);

            aiString filename;
            material->Get(AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), filename);

            aiString none;
            none.Append("None");
            if (matName == none)
            {

            }
            else
            {
                aiString relPath;
                relPath.Append("Assets/Objects/");
                relPath.Append(filename.C_Str());
                SDL_Surface *surf = IMG_Load(relPath.C_Str());

                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surf->w, surf->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surf->pixels);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

                mesh->m_materials.emplace(matName.C_Str(), mat);
            }
        }

        subMesh->material = mesh->m_materials[matName.C_Str()];
        if (curMesh->HasPositions())
        {
            subMesh->size = curMesh->mNumVertices;

            if (i == 0)
            {
                subMesh->offset = 0;
            }
            else
            {
                SubMesh* prev = mesh->m_subMeshes[mesh->m_subMeshes.size() - 1];
                subMesh->offset = prev->offset + prev->size;
            }
        }

        //mesh->m_vertPieces.insert(mesh->m_vertPieces.end(), curMesh->mVertices,
        //	curMesh->mVertices + curMesh->mNumVertices);

        for (unsigned int v = 0; v < curMesh->mNumVertices; ++v)
        {
            mesh->m_vertices.push_back(Vertex{
                { curMesh->mVertices[v].x, curMesh->mVertices[v].y, curMesh->mVertices[v].z },
                { (curMesh->mTextureCoords[0] + v)->x, (curMesh->mTextureCoords[0] + v)->y } });
        }

        mesh->m_subMeshes.push_back(subMesh);
    }

    mesh->Init();
    return mesh;
}
