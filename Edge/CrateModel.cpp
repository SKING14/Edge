#include "CrateModel.h"

#include "Constants.h"
#include "CollisionShapeAABB.h"

CrateModel::CrateModel(int xPos, int yPos)
{
    SDL_Rect collider = { xPos, yPos, Constants::k_doubleFloorSize * 2, Constants::k_doubleFloorSize };
    m_pCollider = new CollisionShapeAABB(collider);
}

CrateModel::~CrateModel()
{
   // delete m_pCollider;
   // m_pCollider = nullptr;
}
