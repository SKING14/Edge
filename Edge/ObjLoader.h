#pragma once

#include <vector>
#include "Mesh.h"

class ObjLoader
{
public:
    ObjLoader();
    ~ObjLoader();

    Mesh* Load(const char *filename);
    void LoadMaterial(Mesh *mesh, const char *filename);

    Mesh* CreateMesh(const char *filename);
};

