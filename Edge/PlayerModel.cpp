#include "PlayerModel.h"

#include "CollisionShapeAABB.h"
#include "CollisionShapeLine.h"
#include "TextFileReader.h"

PlayerModel::PlayerModel(std::string variables)
{
    Init(variables);
}

void PlayerModel::Init(std::string variables)
{
    // Set Starting Position
    std::string specificVariable;
    size_t offset;
    specificVariable = TextFileReader::GetSelectedDataFromString(variables, "Starting Coordinates: ");
    
    // Set Destination Collider
    offset = TextFileReader::GetValueFromString(specificVariable, 0, m_xPos);
    TextFileReader::GetValueFromString(specificVariable, offset, m_yPos);
    m_pDstCollider = new CollisionShapeLine(m_xPos, m_yPos, m_xPos, m_yPos);

    // Set Speed
    specificVariable = TextFileReader::GetSelectedDataFromString(variables, "Speed: ");
    TextFileReader::GetValueFromString(specificVariable, 0, m_playerSpeed);
}

PlayerModel::~PlayerModel()
{
    delete m_pDstCollider;
    m_pDstCollider = nullptr;
}

// If the player attempted to move this will update the model and set player to idle, otherwise player is idle so do nothing
void PlayerModel::Update()
{
    switch (m_nextMove)
    {
    case MOVE(Idle):
        break;
    case MOVE(Up):
        m_yPos -= m_playerSpeed;
        break;
    case MOVE(Down):
        m_yPos += m_playerSpeed;
        break;
    case MOVE(Left):
        m_xPos -= m_playerSpeed;
        break;
    case MOVE(Right):
        m_xPos += m_playerSpeed;
        break;
    default:
        break;
    }
    // Set the player to idle and wait for the next player input
    SetIdle();
}

// The 4 movement functions set the the player facing the direction he will attempt to move when he's updated
void PlayerModel::SeekUp()
{
    m_pFacing = MOVE(Up);
    m_nextMove = MOVE(Up);
}

void PlayerModel::SeekDown()
{
    m_pFacing = MOVE(Down);
    m_nextMove = MOVE(Down);
}

void PlayerModel::SeekLeft()
{
    m_pFacing = MOVE(Left);
    m_nextMove = MOVE(Left);
}

void PlayerModel::SeekRight()
{
    m_pFacing = MOVE(Right);
    m_nextMove = MOVE(Right);
}

void PlayerModel::SetIdle()
{
    m_nextMove = MOVE(Idle);
}

void PlayerModel::SetPosition(const int x, const int y)
{
    m_xPos = x;
    m_yPos = y;
}

// This returns the destination collider (where the player wants to move)
CollisionShapeLine* PlayerModel::GetDstCollider() const
{
    switch (m_pFacing)
    {
    case MOVE(Up):
        m_pDstCollider->SetPoints(m_xPos - 1, m_yPos - 1, (m_xPos + Constants::k_doubleFloorSize + 1), 
            m_yPos - 1);
        break;
    case MOVE(Down):
        m_pDstCollider->SetPoints((m_xPos - 1), (m_yPos + Constants::k_doubleFloorSize + 1), 
            (m_xPos + Constants::k_doubleFloorSize + 1), (m_yPos + Constants::k_doubleFloorSize + 1));
        break;
    case MOVE(Left):
        m_pDstCollider->SetPoints(m_xPos - 1, m_yPos - 1, 
            m_xPos - 1, (m_yPos + Constants::k_doubleFloorSize + 1));
        break;
    case MOVE(Right):
        m_pDstCollider->SetPoints((m_xPos + Constants::k_doubleFloorSize + 1), m_yPos - 1, 
            m_xPos + Constants::k_doubleFloorSize + 1, (m_yPos + Constants::k_doubleFloorSize + 1));
        break;
    case MOVE(Idle):
        break;
    default:
        break;
    }
    return m_pDstCollider;
}