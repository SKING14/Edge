#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <iostream>

#include "Constants.h"
#include "GameStateMachine.h"
#include "EventSystem.h"
#include "GLRenderer.h"
#include <vld.h>

// Crate destructor, Wall, 

int main(int argc, char* argv[])
{
	// Initializing the SDL Library for use
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "SDL_Init failure: " << SDL_GetError() << std::endl;
        return 1;
    }

    // Creates the game window
    SDL_Window* win = SDL_CreateWindow("Edge", Constants::k_winX, Constants::k_winY, Constants::k_winWidth, Constants::k_winHeight, SDL_WINDOW_OPENGL);

    // Make sure the game window was created properly
    if (win == nullptr)
    {
        std::cout << "SDL_CreateWindow failure: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    // Creates the renderer for the game
    SDL_Renderer* ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    // Make sure the renderer was created properly
    if (ren == nullptr)
    {
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateRenderer failure: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    // Sound initialization
    if (Mix_Init(MIX_INIT_OGG) != 0)
    {
        std::cout << "SDL_Mixer failure: " << SDL_GetError() << std::endl;
    }

    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024);

    TTF_Init();

   /* GLRenderer glRenderer;
    glRenderer.Init(win);

    while (true)
    {
        glRenderer.Draw();
    }
  */
    // Create the Event System
    EventSystem eventSystem;

    // The main game
    GameStateMachine game(&eventSystem, ren);
    game.ChangeState(GameStateMachine::State::Menu);

    // The main game loop
    while (game.Update());
    
    // Cleanup code
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    SDL_AudioQuit();
    TTF_Quit();
    SDL_Quit();
    
    return 0;
}