#include "WeightRoomController.h"

#include "MasterSystem.h"
#include "HallwayEvent.h"
#include "ButtonEvent.h"

WeightRoomController::WeightRoomController(EventSystem *pEventSystem, SDL_Renderer *pRen, PlayerController *pPlayerController, MasterSystem *pMasterSystem)
    : RoomController(pEventSystem, pRen, pPlayerController, VARIABLES("WeightRoomWalls"), pMasterSystem)
    , m_recDoorController(pRen, VARIABLES("WeightToRecController"), EventIDManager::WeightToRecButtonEventID)
    , m_rightSkyDoorController(pRen, VARIABLES("WeightToRightSkyController"), EventIDManager::WeightToRightSkyButtonEventID)
{
    Init();
    AddEvents();
    AddListeners();
    GenerateTriggers();
}

void WeightRoomController::Init()
{
    SetRoom(RoomController::WeightRoom);
    m_pMasterSystem->AddRoom(this);
    m_pMasterSystem->AddDoor(m_recDoorController.GetModel());
    m_pMasterSystem->AddDoor(m_rightSkyDoorController.GetModel());
    
    m_doors.push_back(&m_recDoorController);
    m_doors.push_back(&m_rightSkyDoorController); 
}

WeightRoomController::~WeightRoomController()
{
    CleanupEvents();
}

void WeightRoomController::GenerateTriggers()
{
    m_triggers.insert(std::make_pair(m_pEnterRecRoomEvent, m_recDoorController.GetEnterTrigger()));
    m_triggers.insert(std::make_pair(m_pEnterRightSkyRoomEvent, m_rightSkyDoorController.GetEnterTrigger()));

    m_buttons.insert(std::make_pair(m_pRecRoomButtonEvent, m_recDoorController.GetButtonTrigger()));
    m_buttons.insert(std::make_pair(m_pRightSkyRoomButtonEvent, m_rightSkyDoorController.GetButtonTrigger()));
}

void WeightRoomController::CleanupEvents()
{
    delete m_pEnterRecRoomEvent;
    m_pEnterRecRoomEvent = nullptr;
    delete m_pEnterRightSkyRoomEvent;
    m_pEnterRightSkyRoomEvent = nullptr;
    delete m_pRecRoomButtonEvent;
    m_pRecRoomButtonEvent = nullptr;
    delete m_pRightSkyRoomButtonEvent;
    m_pRightSkyRoomButtonEvent = nullptr;

    m_pEventSystem->RemoveListener(ID(EnterRecFromWeightEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(EnterRecFromWeightEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(ID(EnterRightSkyFromWeightEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(ID(EnterRightSkyFromWeightEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(WeightToRecButtonEventID), m_pMasterSystem->GetDoorSystem());
    m_pEventSystem->RemoveListener(ID(WeightToRightSkyButtonEventID), m_pMasterSystem->GetDoorSystem());
}

void WeightRoomController::CheckDialogueBoxes()
{
}

void WeightRoomController::RenderUniqueRoomObjects()
{
}

void WeightRoomController::AddEvents()
{
    m_pEnterRecRoomEvent = new HallwayEvent(EventIDManager::EnterRecFromWeightEventID);
    m_pEnterRightSkyRoomEvent = new HallwayEvent(EventIDManager::EnterRightSkyFromWeightEventID);
    m_pRecRoomButtonEvent = new ButtonEvent(EventIDManager::WeightToRecButtonEventID);
    m_pRightSkyRoomButtonEvent = new ButtonEvent(EventIDManager::WeightToRightSkyButtonEventID);
}

void WeightRoomController::AddListeners()
{
    m_pEventSystem->AddListener(ID(EnterRecFromWeightEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(EnterRecFromWeightEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(ID(EnterRightSkyFromWeightEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(ID(EnterRightSkyFromWeightEventID), m_pMasterSystem->GetPlayerSystem());

    m_pEventSystem->AddListener(ID(WeightToRecButtonEventID), m_pMasterSystem->GetDoorSystem());
    m_pEventSystem->AddListener(ID(WeightToRightSkyButtonEventID), m_pMasterSystem->GetDoorSystem());
}

void WeightRoomController::TriggerProceedEvents()
{
}