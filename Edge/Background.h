#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include "Constants.h"

class Background
{
private:
    SDL_Renderer* m_pRen{ nullptr };
    SDL_Texture* m_pBG{ nullptr };
    const char* m_pBGFilename{ "Assets/Images/TitleScreenBG.jpg" };
    SDL_Rect m_src{ 0, 0, Constants::k_winWidth, Constants::k_winHeight};
    SDL_Rect m_dst{ 0, 0, Constants::k_winWidth, Constants::k_winHeight};
public:
    Background(SDL_Renderer* pRen);
    ~Background();

    void Render();
};