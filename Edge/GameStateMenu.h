#pragma once
#include "GameState.h"

#include "Constants.h"
#include "EventSystem.h"
#include "MenuController.h"
#include "Image.h"
#include "SelectionBox.h"
#include "DialogueBox.h"

class SelectionBoxSystem;
class AudioSystem;
class MenuEvent;

class GameStateMenu : public GameState
{
private:
    // Event variables
    EventSystem* m_pEventSystem{ nullptr };
    SelectionBoxSystem* m_pSelectionBoxSystem{ nullptr };
    AudioSystem* m_pAudioSystem{ nullptr };
    MenuEvent *m_pUpKeyPressedEvent{ nullptr };
    MenuEvent *m_pDownKeyPressedEvent{ nullptr };
    MenuEvent *m_pSelectionMadeEvent{ nullptr };

    // Game Component Variables
    SDL_Renderer* m_pRen{ nullptr };
    MenuController m_user;
    Image m_bG;
    Image m_title;
    SelectionBox m_selectBox;
    DialogueBox m_controls;
    DialogueBox m_credits;
    int m_selection{ -1 };

public:
    GameStateMenu(EventSystem* eventSystem, SDL_Renderer* ren);
    ~GameStateMenu();

    void Enter(GameStateMachine* gsm) override;
    bool Update() override;
    void Exit() override;

private:
    void SetupEvents();
    void CleanupEvents();
    void DestroyObjects();
    bool GetUserChoice();
    void ExecuteSelection();
};