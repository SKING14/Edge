#pragma once

#include <SDL.h>
#include <vector>
#include "Image.h"

class View
{
protected:
    std::vector<Image> m_timeline;
    int m_currentAnimation{ 0 };
    int m_timePerFrame{ 0 };
    int m_myTimer{ std::numeric_limits<int>::max() };

public:
    View() {}
    View(const char * pFilename, SDL_Renderer * pRen, SDL_Rect dimensions);
    void CreateAnimationFromMultipleSources(const char * pFilename, SDL_Renderer * pRen, SDL_Rect dimensions);
    virtual ~View() {}

    virtual void Update() {}
    virtual void Render();
    virtual void Render(int xPos, int Ypos) {}
};
