#include "MasterSystem.h"


MasterSystem::MasterSystem(PlayerController *pController)
    : m_audioSystem()
    , m_hallwaySystem()
    , m_doorSystem()
    , m_playerSystem(pController)
{
}

void MasterSystem::AddRoom(RoomController * pRoom)
{
    m_hallwaySystem.SetToRoom(pRoom);
}

void MasterSystem::AddDoor(DoorModel * pDoor)
{
    m_doorSystem.AddDoorToSystem(pDoor);
}