#include "Mesh.h"



Mesh::Mesh()
{
}


Mesh::~Mesh()
{
}

void Mesh::Init()
{
    //m_vertices.clear();
    //m_vertices.reserve(vertArraySize);

    //m_indices.clear();
    //m_indices.reserve(indexArraySize);

    //for (GLuint i = 0; i < vertArraySize; ++i)
    //{
    //	m_vertices.push_back(vertArray[i]);
    //}

    //for (GLuint i = 0; i < indexArraySize; ++i)
    //{
    //	m_indices.push_back(indexArray[i]);
    //}

    glGenVertexArrays(1, &m_vaoId);
    glBindVertexArray(m_vaoId);

//    GLuint vboId, iboId;
    // Vertex Buffer Object
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_vertices.size(),
        &m_vertices[0], GL_DYNAMIC_DRAW);

    // Index Buffer Object
    //glGenBuffers(1, &iboId);
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) *
    //	m_indices.size(), &m_indices[0], GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    // Vertex Attributes
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        (GLvoid*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        (GLvoid*)sizeof(glm::vec3));

    // Safe to unbind our objects.  We don't want to accidentally
    //	modify this mesh's buffers elsewhere
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}
