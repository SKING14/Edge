#pragma once

// This is the user for the main menu
class MenuController
{
public:
    MenuController() {}
    ~MenuController() {}

    const short int GetUserInput();
};