#pragma once

#include <vector>
#include <SDL.h>

class Image;

class AnimationStation
{
public:
    static void CreateAnimation(SDL_Renderer *pRen, 
        const char* pFilename, std::vector<Image>& timeline, SDL_Rect dst, int &timePerFrame);
    static void CreateAnimationFromMultipleSources(SDL_Renderer *pRen,
        const char* pFilename, std::vector<Image>& timeline, SDL_Rect dst, int &timePerFrame);
    static SDL_Texture* SetTexture(SDL_Renderer * pRen, std::string& animationVariables, std::string nameOfImageHeader);
};