#include "Trigger.h"

#include "Constants.h"
#include "CollisionShapeAABB.h"
#include "TextFileReader.h"

#define DEBUGGING 0

Trigger::Trigger(const char* pFilename, SDL_Renderer* pRen)
	: m_pImage(VARIABLES("Trigger"), pRen)
{
	SDL_Rect dimensions;
    std::string entireString;
    TextFileReader::ReadEntireTextToString(pFilename, entireString);
    TextFileReader::GetRecFromString(entireString, dimensions, (std::string)"Destination: ");

#if DEBUGGING
	m_pImage.SetDstRect(dimensions);
#endif

    m_pCollider = new CollisionShapeAABB(dimensions);
}

Trigger::~Trigger()
{
    delete m_pCollider;
    m_pCollider = nullptr;
}

void Trigger::Render()
{
#if DEBUGGING
    m_pImage.Render();
#endif
}