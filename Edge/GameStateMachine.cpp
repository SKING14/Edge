#include "GameStateMachine.h"

#include "GameStateMenu.h"
#include "GameStatePlay.h"
#include "GameStateGl.h"
#include "GameStateGameOver.h"
#include "EventSystem.h"

GameStateMachine::GameStateMachine(EventSystem* pEventSystem, SDL_Renderer* pRen)
    : m_pEventSystem(pEventSystem)
    , m_pRen(pRen)
{
}

GameStateMachine::~GameStateMachine()
{
    delete m_pCurState;
    m_pCurState = nullptr;
}

// Game State Cycle: (1) Exit the previous Game State and call it's method Exit()
// (2) Switch to new Game State
// (3) Enter that game state
void GameStateMachine::ChangeState(State newState)
{
    if (m_pCurState != nullptr)
    {
        m_pCurState->Exit();
        delete m_pCurState;
        m_pCurState = nullptr;
    }

    switch (newState)
    {
    case State::Menu:
        m_pCurState = new GameStateMenu(m_pEventSystem, m_pRen);
        break;
    case State::Play:
        m_pCurState = new GameStatePlay(m_pEventSystem, m_pRen);
        break;
    case State::GL:
        m_pCurState = new GameStateGL();
        break;
    case State::GameOver:
        m_pCurState = new GameStateGameOver();
        break;
    }

    m_pCurState->Enter(this);
}

// Calls the Update() for the current Game State
bool GameStateMachine::Update()
{
    return m_pCurState->Update();
}