#include "GameStateMenu.h"

#include "GameStateMachine.h"
#include "SelectionBoxSystem.h"
#include "AudioSystem.h"
#include "MenuEvent.h"
#include "EventIDManager.h"

GameStateMenu::GameStateMenu(EventSystem* eventSystem, SDL_Renderer* ren)
    : m_pEventSystem(eventSystem)
    , m_pRen(ren)
    , m_bG(VARIABLES("TitleScreenBG"), m_pRen)
    , m_title(VARIABLES("Title"), m_pRen)
    , m_selectBox(VARIABLES("SelectionBox"), m_pRen)
    , m_controls (VARIABLES("Controls"), m_pRen)
    , m_credits (VARIABLES("Credits"), m_pRen)
{
    SetupEvents();
}

void GameStateMenu::SetupEvents()
{
    // Event Listeners
    m_pSelectionBoxSystem = new SelectionBoxSystem(&m_selectBox);
    m_pAudioSystem = new AudioSystem();

    // Events
    m_pUpKeyPressedEvent = new MenuEvent(ID(UpKeyPressedEventID));
    m_pDownKeyPressedEvent = new MenuEvent(ID(DownKeyPressedEventID));
    m_pSelectionMadeEvent = new MenuEvent(ID(SelectionMadeEventID));

    // Add listeners for specific events
    m_pEventSystem->AddListener(ID(UpKeyPressedEventID), m_pSelectionBoxSystem);
    m_pEventSystem->AddListener(ID(UpKeyPressedEventID), m_pAudioSystem);
    m_pEventSystem->AddListener(ID(DownKeyPressedEventID), m_pSelectionBoxSystem);
    m_pEventSystem->AddListener(ID(DownKeyPressedEventID), m_pAudioSystem);
    m_pEventSystem->AddListener(ID(SelectionMadeEventID), m_pAudioSystem);
}

GameStateMenu::~GameStateMenu()
{
    CleanupEvents();
    DestroyObjects();
}

void GameStateMenu::CleanupEvents()
{
    delete m_pUpKeyPressedEvent;
    m_pUpKeyPressedEvent = nullptr;
    delete m_pDownKeyPressedEvent;
    m_pDownKeyPressedEvent = nullptr;
    delete m_pSelectionMadeEvent;
    m_pSelectionMadeEvent = nullptr;

    // Cleanup and remove event listeners
    m_pEventSystem->RemoveListener(ID(UpKeyPressedEventID), m_pSelectionBoxSystem);
    m_pEventSystem->RemoveListener(ID(UpKeyPressedEventID), m_pAudioSystem);
    m_pEventSystem->RemoveListener(ID(DownKeyPressedEventID), m_pSelectionBoxSystem);
    m_pEventSystem->RemoveListener(ID(DownKeyPressedEventID), m_pAudioSystem);
    m_pEventSystem->RemoveListener(ID(SelectionMadeEventID), m_pAudioSystem);
}

void GameStateMenu::DestroyObjects()
{
    delete m_pSelectionBoxSystem;
    m_pSelectionBoxSystem = nullptr;
    delete m_pAudioSystem;
    m_pAudioSystem = nullptr;
}

// We send the background, title, and selection text to the renderer
void GameStateMenu::Enter(GameStateMachine* gsm)
{
    GameState::Enter(gsm);
    m_bG.Render();
    m_title.Render();
    m_selectBox.Render();
    SDL_RenderPresent(m_pRen);
}

// Highlights the selection the user is on 
bool GameStateMenu::Update()
{
    if (!GetUserChoice())
    {
        m_selectBox.Render();
        m_controls.Render();
        m_credits.Render();
        SDL_RenderPresent(m_pRen);
    }
    ExecuteSelection();
    return m_isPlaying;
}

// Returns true if user selected a choice (changing game state)
bool GameStateMenu::GetUserChoice()
{
    short int selectionMovement = m_user.GetUserInput();

    if (selectionMovement == Constants::CursorDirections::ScrollUp)
    {
        m_pEventSystem->TriggerEvent(m_pUpKeyPressedEvent);
        return false;
    }
    else if (selectionMovement == Constants::CursorDirections::ScrollDown)
    {
        m_pEventSystem->TriggerEvent(m_pDownKeyPressedEvent);
        return false;
    }
    // Change state based on what selection the user is currently highlighting and return (we do NOT want to render)
    else if (selectionMovement == Constants::CursorDirections::Select)
    {
        m_pEventSystem->TriggerEvent(m_pSelectionMadeEvent);
        m_selection = m_selectBox.SelectionMade();
        return true;
    }
    else
    {
        return false;
    }
}

void GameStateMenu::Exit()
{
    SDL_RenderClear(m_pRen);
}

// Enters the game state based on which option the user selected
void GameStateMenu::ExecuteSelection()
{
    switch (m_selection)
    {
    case 0:
        m_pStateMachine->ChangeState(GameStateMachine::State::Play);
        break;
    case 1:
      //  m_pStateMachine->ChangeState(GameStateMachine::State::GL);
        break;
    case 2:
        m_controls.Enable();
        break;
    case 3:
        m_credits.Enable();
        break;
    case 4:
        m_isPlaying = false;
        break;
    default:
        break;
    }
}