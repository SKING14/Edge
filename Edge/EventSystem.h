#pragma once

#include <map>
#include <list>

class EventListener;
class Event;

class EventSystem
{
public:
    typedef unsigned int EventID;

private:
    typedef std::list<EventListener*> ListenerList;
    typedef std::map<EventID, ListenerList> Listeners;

    Listeners m_listeners;

public:
    EventSystem() {}
    ~EventSystem() {}

    // Listener Management
    void AddListener(EventID id, EventListener* pListener);
    void RemoveListener(EventID id, EventListener* pListener);

    // Event Management
    void TriggerEvent(const Event* pEvent);
};