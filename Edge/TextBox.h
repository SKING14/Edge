#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

class TextBox
{
protected:
    SDL_Renderer *m_pRen;
    TTF_Font *m_pFont;
    SDL_Color m_textColor;
    std::string m_pTextFile = "Assets/Texts/" ;
    std::string m_pFontFile =  "Assets/Fonts/" ;
    int m_fontSize;

public:
    TextBox(const char *pFilename, SDL_Renderer *pRen);
    virtual ~TextBox() {}

private:
    void Init(const char *pFilename);
    void LoadFont();
};