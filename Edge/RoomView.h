#pragma once
#include "View.h"

#include <vector>

#include "Image.h"

class Wall;
class DoorController;
class DoorModel;
class DoorView;
class CrateModel;

class RoomView : public View
{
private:
    SDL_Renderer *m_pRen;
    Image m_pFloor;
    Image m_pCornerWallIMG;
    Image m_pLeftRightWallIMG;
    Image m_pUpDownWallIMG;
    Image m_pCrateIMG;
    std::vector<Image> m_wallImages;
    std::vector<Image> m_crateImages;

public:
    RoomView(SDL_Renderer *pRen, std::vector<Wall> &walls, std::vector<CrateModel> crates);
    ~RoomView() {}

    virtual void Render();
    void RenderDoors(std::vector<DoorController*> &doors);
    void CrateDestroyed(int index);

private:
    void LoadImages();
    void SetWalls(std::vector<Wall>& walls);
    void SetCrates(std::vector<CrateModel>& crates);

    template<typename T>
    void RoomView::RenderGroup(std::vector<T> group)
    {
        for (auto item : group)
        {
            item.Render();
        }
    };
};