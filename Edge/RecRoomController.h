#pragma once
#include "RoomController.h"

#include "DoorController.h"

class HallwayEvent;
class ButtonEvent;

class RecRoomController : public RoomController
{
private:
    HallwayEvent *m_pEnterPlayersRoomEvent;
    HallwayEvent *m_pEnterWeightRoomEvent;

    ButtonEvent *m_pRecToPlayerButtonEvent;
    ButtonEvent *m_pRecToWeightButtonEvent;

    DoorController m_playerDoorController;
    DoorController m_weightDoorController;
    
public:
    RecRoomController(EventSystem *pEventSystem, SDL_Renderer *pRen, 
        PlayerController *pPlayerController, MasterSystem *pMasterSystem);
    ~RecRoomController() override;

private:
    void Init() override;
    void GenerateTriggers() override;
    void CleanupEvents() override;
    void TriggerProceedEvents() override;
	void RenderUniqueRoomObjects() override;
	void AddEvents() override;
	void AddListeners() override;
    void CheckDialogueBoxes() override {}

};