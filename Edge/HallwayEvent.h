#pragma once
#include "Event.h"

class RoomController;

class HallwayEvent : public Event
{
private:
    RoomController* m_room;
public:
    HallwayEvent(unsigned int ID) : Event(ID) {}
    ~HallwayEvent() {}
};