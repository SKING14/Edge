#include "PlayersRoomController.h"

#include <cassert>

#include "MasterSystem.h"
#include "CollisionShapeAABB.h"
#include "RoomModel.h"
#include "DialogueBoxSystem.h"
#include "ButtonEvent.h"
#include "PlayerEvent.h"
#include "HallwayEvent.h"
#include "DialogueBoxEvent.h"
#include "DialogueBox.h"
#include "Trigger.h"
#include "PlayerController.h"
#include "EventIDManager.h"
#include "ButtonView.h"

// Starting level of the game
PlayersRoomController::PlayersRoomController(EventSystem* pEventSystem, SDL_Renderer* pRen, PlayerController* pPlayerController, MasterSystem *pMasterSystem)
    : RoomController(pEventSystem, pRen, pPlayerController, VARIABLES("PlayerRoomWalls"), pMasterSystem)
    , m_pIntroTrigger(VARIABLES("IntroTrigger"), m_pRen)
    , m_pIntro(VARIABLES("Intro"), m_pRen)
    , m_recDoorController(pRen, VARIABLES("PlayerToRecController"), EventIDManager::PlayersToRecButtonEventID)
{
    Init();
}

void PlayersRoomController::Init()
{
    SetRoom(RoomController::PlayersRoom);
    m_pMasterSystem->AddRoom(this);
    m_pMasterSystem->AddDoor(m_recDoorController.GetModel());
    m_dialogueBoxes.push_back(&m_pIntro);
    m_doors.push_back(&m_recDoorController);
    AddEvents();
    AddListeners();
    GenerateTriggers();

}

void PlayersRoomController::GenerateTriggers()
{
    m_triggers.insert(std::make_pair(m_pIntroTriggerEnterEvent, &m_pIntroTrigger));
    m_triggers.insert(std::make_pair(m_pEnterRecRoomEvent, m_recDoorController.GetEnterTrigger()));
    m_buttons.insert(std::make_pair(m_pRecDoorButtonEvent, m_recDoorController.GetButtonTrigger()));
}

PlayersRoomController::~PlayersRoomController()
{
    CleanupEvents();
}

void PlayersRoomController::CleanupEvents()
{   
    delete m_pEnterRecRoomEvent;
    m_pEnterRecRoomEvent = nullptr;
    delete m_pPlayerProceedEvent;
    m_pPlayerProceedEvent = nullptr;
    delete m_pIntroTriggerEnterEvent;
    m_pIntroTriggerEnterEvent = nullptr;
    delete m_pRecDoorButtonEvent;
    m_pRecDoorButtonEvent = nullptr;
    delete m_pExitDialogueBoxEvent;
    m_pExitDialogueBoxEvent = nullptr;

    m_pEventSystem->RemoveListener(ID(IntroTriggerEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(IntroTriggerEventID), m_pDialogueBoxSystem);
    m_pEventSystem->RemoveListener(ID(PlayerProceedEventID), m_pDialogueBoxSystem);
    m_pEventSystem->RemoveListener(ID(ExitDialogueBoxEventID), m_pMasterSystem->GetAudioSystem());
    m_pEventSystem->RemoveListener(ID(ExitDialogueBoxEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(EnterRecFromPlayersEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(ID(EnterRecFromPlayersEventID), m_pMasterSystem->GetPlayerSystem());
	m_pEventSystem->RemoveListener(ID(PlayersToRecButtonEventID), m_pMasterSystem->GetDoorSystem());

    delete m_pDialogueBoxSystem;
    m_pDialogueBoxSystem = nullptr;
}

void PlayersRoomController::RenderUniqueRoomObjects()
{
    m_pIntro.Render();
}

void PlayersRoomController::AddEvents()
{
    m_pIntroTriggerEnterEvent = new DialogueBoxEvent(ID(IntroTriggerEventID));
    m_pPlayerProceedEvent = new PlayerEvent(ID(PlayerProceedEventID));
    m_pEnterRecRoomEvent = new HallwayEvent(ID(EnterRecFromPlayersEventID));
    m_pRecDoorButtonEvent = new ButtonEvent(ID(PlayersToRecButtonEventID));
    m_pExitDialogueBoxEvent = new DialogueBoxEvent(ID(ExitDialogueBoxEventID));
}

void PlayersRoomController::AddListeners()
{
    // Create Listeners
    m_pDialogueBoxSystem = new DialogueBoxSystem(&m_pIntro);

    // Add Listeners
    m_pEventSystem->AddListener(ID(IntroTriggerEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(IntroTriggerEventID), m_pDialogueBoxSystem);
    m_pEventSystem->AddListener(ID(PlayerProceedEventID), m_pDialogueBoxSystem);
    m_pEventSystem->AddListener(ID(ExitDialogueBoxEventID), m_pMasterSystem->GetAudioSystem());
    m_pEventSystem->AddListener(ID(ExitDialogueBoxEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(EnterRecFromPlayersEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(ID(EnterRecFromPlayersEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(PlayersToRecButtonEventID), m_pMasterSystem->GetDoorSystem());
}

void PlayersRoomController::CheckDialogueBoxes()
{
    for (int i{ 0 }; i < static_cast<int>(m_dialogueBoxes.size()); ++i)
    {
        if (m_dialogueBoxes[i]->CheckIfRead())
        {
            m_pEventSystem->TriggerEvent(m_pExitDialogueBoxEvent);
            m_dialogueBoxes.erase(m_dialogueBoxes.begin() + i);
        }
        else
        {
            ++i;
        }
    }
}

void PlayersRoomController::TriggerProceedEvents()
{
    m_pEventSystem->TriggerEvent(m_pPlayerProceedEvent);
}
