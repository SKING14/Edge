#pragma once
#include "Model.h"

#include <vector>
#include <unordered_map>
#include "Wall.h"
#include "CrateModel.h"

class CollisionShape;
class CrateModel;

class RoomModel : public Model
{
private:
    std::vector<std::string> m_wallTypes{};
    std::vector<Wall> m_walls{};
    std::vector<CrateModel> m_crates{};

public:
    RoomModel(const char * pFilename);
    virtual ~RoomModel() {}
    
    bool CheckAllCollisions(CollisionShape * pCollider);
    bool CheckWallCollisions(CollisionShape *pCollider);
    bool CheckCrateCollisions(CollisionShape * pCollider);
    std::vector<Wall>& GetWalls() { return m_walls; }
    std::vector<CrateModel>& GetCrates() { return m_crates; }

private:
    template <typename T>
    bool CheckCollisions(CollisionShape* pCollider, std::vector<T> colliders)
    {
        for (unsigned int index{ 0 }; index < colliders.size(); ++index)
        {
            if (CollisionSystem::AreOverlapping(pCollider, colliders[index].GetCollider()))
            {
                return true;
            }
        }
        return false;
    }

    void GenerateWalls();
};