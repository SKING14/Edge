#pragma once
#include "EventListener.h"

class Event;
class RoomController;

class HallwaySystem: public EventListener
{
private:
    RoomController *m_pRoom;
public:
    HallwaySystem() {}
     ~HallwaySystem() {}

	void HandleEvent(const Event * pEvent) override;
    void SetToRoom(RoomController * pRoom) { m_pRoom = pRoom; }
};