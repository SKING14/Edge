#include "PlayerSystem.h"

#include "PlayerController.h"
#include "HallwayEvent.h"

PlayerSystem::PlayerSystem(PlayerController *pPlayer)
    : m_pPlayer(pPlayer)
{
}

void PlayerSystem::HandleEvent(const Event *pEvent)
{
    switch (pEvent->GetID())
    {
    case EventIDManager::IntroTriggerEventID:
        m_pPlayer->Pause();
        break;
    case EventIDManager::EnterRecFromPlayersEventID:
        m_pPlayer->SetAtStartPosition(64, 600);
        break;
    case EventIDManager::EnterPlayersFromRecEventID:
        m_pPlayer->SetAtStartPosition(960, 600);
        break;
    case EventIDManager::EnterWeightFromRecEventID:
        m_pPlayer->SetAtStartPosition(800, 650);
        break;
    case EventIDManager::EnterRecFromWeightEventID:
        m_pPlayer->SetAtStartPosition(800, 64);
        break;
    case EventIDManager::EnterRightSkyFromWeightEventID:
        m_pPlayer->SetAtStartPosition(640, 700);
        break;
    case EventIDManager::EnterRightWeightFromSkyEventID:
        m_pPlayer->SetAtStartPosition(640, 100);
        break;
    case EventIDManager::ExitDialogueBoxEventID:
        m_pPlayer->Unpause();
        break;
    default:
        break;
    }
}