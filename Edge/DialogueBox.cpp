#include "DialogueBox.h"

#include <cassert>
#include <iostream>

#include "Constants.h"
#include "TextFileReader.h"

DialogueBox::DialogueBox(const char *pFilename, SDL_Renderer *pRen)
    : TextBox(pFilename, pRen)
{
    DialogueInit(pFilename);
}

void DialogueBox::DialogueInit(const char *pFilename)
{
    std::string entireText;
    std::string variable;
    
    TextFileReader::ReadEntireTextToString(pFilename, entireText);
    TextFileReader::GetColorFromString(entireText, m_borderColor, (std::string)"Border Color: ");
    TextFileReader::GetColorFromString(entireText, m_bgColor, (std::string)"Background Color: ");
    TextFileReader::GetRecFromString(entireText, m_borderRect, (std::string)"Border Rect: ");
    TextFileReader::GetRecFromString(entireText, m_bgRect, (std::string)"Background Rect: ");

    variable = TextFileReader::GetSelectedDataFromString(entireText, "Max Lines: ");
    TextFileReader::GetValueFromString(variable, 0, m_maxLines);
    variable = TextFileReader::GetSelectedDataFromString(entireText, "Max Line Length: ");
    TextFileReader::GetValueFromString(variable, 0, m_maxCharsPerLine);

    const char *pText = m_pTextFile.c_str();
    TextFileReader::GetDialogueFromText(pText, m_lines, m_maxCharsPerLine);

}

DialogueBox::~DialogueBox()
{
    //for (int i{ 0 }; i < m_lines.size(); ++i)
    //{
    //    // delete m_lines[i].first;
    //   // m_lines[i].first = nullptr;
    //}
}

void DialogueBox::SetupBox()
{
    // Setup Border of Text Box
    SDL_SetRenderDrawColor(m_pRen, m_borderColor.r, m_borderColor.g, m_borderColor.b, m_borderColor.a);
    SDL_Rect textBoxBorder{
        HALF(Constants::k_winWidth) - 320,
        Constants::k_winHeight - 240,
        640,
        200
    };
    SDL_RenderFillRect(m_pRen, &textBoxBorder);

    // Setup Background Color of Text Box
    SDL_SetRenderDrawColor(m_pRen, m_bgColor.r, m_bgColor.g, m_bgColor.b, m_bgColor.a);
    SDL_Rect textBoxBackground{
        HALF(Constants::k_winWidth) - 320 + 10,
        Constants::k_winHeight - 240 + 10,
        620,
        180
    };
    SDL_RenderFillRect(m_pRen, &textBoxBackground);
}

void DialogueBox::SetupLines()
{
    int lineCount{ 0 };

    for (int i{ m_boxesRead * 8 }; i < static_cast<int>(m_lines.size()); ++i)
    {
        // Create Line
        SDL_Surface *textSurface = TTF_RenderText_Solid(m_pFont, m_lines[i].first, m_textColor);
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(m_pRen, textSurface);

        // Setup Text Rect
        SDL_Rect textRect{
            HALF(Constants::k_winWidth) - 320 + 30,
            (Constants::k_winHeight - 240 + 30) + (lineCount * m_fontSize),
            m_lines[i].second * m_fontSize,
            m_fontSize };

        // Copy Line
        SDL_RenderCopy(m_pRen, textTexture, nullptr, &textRect);

        // Free the surface
        SDL_FreeSurface(textSurface);
        ++lineCount;

        if (lineCount >= m_maxLines)
        {
            // If we show that an additional box is not required, then we need to correct to to be true,
            // also increase the box count. I was having issues here, the box count kept increasing too quickly, 
            // I had to add a check so it would only increase once
            if (!m_additionalBoxRequired)
            {
                m_additionalBoxRequired = true;
                ++m_boxCount;
            }
            break;
        }
    }
}

void DialogueBox::Enable()
{
    (m_read) ? (m_enabled = false) : (m_enabled = true);
}

void DialogueBox::MarkAsRead()
{
    if (m_enabled)
    {
        if (m_boxesRead >= m_boxCount)
        {
            m_enabled = false;
            m_read = true;
        }
        ++m_boxesRead;
        m_additionalBoxRequired = false;
    }
}

bool DialogueBox::CheckIfRead() const
{
    return (m_boxesRead >= m_boxCount);
}

void DialogueBox::Render()
{
    if (m_enabled)
    {
        if (m_boxesRead < m_boxCount)
        {
            SetupBox();
            SetupLines();
        }
    }
}

