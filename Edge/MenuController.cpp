#include "MenuController.h"

#include <SDL.h>

#include "Constants.h"

// Get the user's input for scrolling up or down through options
const short int MenuController::GetUserInput()
{
    SDL_Event eve;
    
    SDL_PollEvent(&eve);

    switch (eve.type)
    {
    case SDL_KEYDOWN:
        switch (eve.key.keysym.sym)
        {
        case SDLK_UP:
            return Constants::CursorDirections::ScrollUp;
            break;
        case SDLK_DOWN:
            return Constants::CursorDirections::ScrollDown;
            break;
        case SDLK_RETURN:
            return Constants::CursorDirections::Select;
            break;
        default:
            break;
        }
    default:
        break;
    }
    return -1;
}