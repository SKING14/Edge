#include "BombView.h"

BombView::BombView(const char * pFilename, SDL_Renderer * pRen, SDL_Rect dimensions)
{
    CreateAnimationFromMultipleSources(pFilename, pRen, dimensions);
    m_myTimer = SDL_GetTicks();
}

bool BombView::AnimationCompleted()
{
    return (m_currentAnimation >= m_timeline.size() - 1);
}
