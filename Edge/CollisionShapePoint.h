#pragma once
#include "CollisionShape.h"

class CollisionShapePoint : public CollisionShape
{
private:
    SDL_Point m_point;
public:
    CollisionShapePoint(int x, int y) { m_point = { x, y }; m_type = Type::Point; }
    ~CollisionShapePoint() {}

    // Mutators
    void SetPoint(int x, int y) { m_point = { x, y }; }

    // Accessors
    int GetX() const { return m_point.x; }
    int GetY() const { return m_point.y; }
};