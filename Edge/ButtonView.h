#pragma once
#include "View.h"

#include <SDL.h>
#include "Image.h"

class ButtonView : public View
{
private:
    SDL_Rect src;
    Image m_image;

public:
    ButtonView(const char *pFilename, SDL_Renderer *pRen, SDL_Rect dst) : m_image(pFilename, pRen, dst) {}
    ~ButtonView() {}

    void Render() { m_image.Render(); }
};
