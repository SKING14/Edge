#include "PlayerController.h"

#include "PlayerModel.h"
#include "PlayerView.h"
#include "TextFileReader.h"
#include <string>
#include "CollisionShapeLine.h"
#include "CollisionShapeAABB.h"

// The controller knows about the view and the model, the view knows nothing about the model and the model knows nothing about the view
PlayerController::PlayerController(SDL_Renderer* pRen)
{
    Init(pRen);
}

void PlayerController::Init(SDL_Renderer *pRen)
{
    std::string m_variables;
    TextFileReader::ReadEntireTextToString(VARIABLES("Player"), m_variables);

    m_pPlayerView = new PlayerView(pRen, m_variables);
    m_pPlayerModel = new PlayerModel(m_variables);
}

PlayerController::~PlayerController()
{
    delete m_pPlayerView;
    m_pPlayerView = nullptr;
    delete m_pPlayerModel;
    m_pPlayerModel = nullptr;
}

// Get input from the player (user)
Constants::PlayerActions PlayerController::Input()
{
    SDL_Event eve;
    SDL_PollEvent(&eve);

    switch (eve.type)
    {
    case SDL_KEYDOWN:
        if (!m_paused)
        {
            CheckMovementInput(eve);
            AnimatePlayerView();
        }
        return CheckActionInput(eve);
    default:
        break;
    }
    return PLAYERACTION(NoAction);
}

void PlayerController::CheckMovementInput(SDL_Event eve)
{
    switch (eve.key.keysym.sym)
    {
    case SDLK_UP:
        m_pPlayerModel->SeekUp();
        break;
    case SDLK_DOWN:
        m_pPlayerModel->SeekDown();
        break;
    case SDLK_LEFT:
        m_pPlayerModel->SeekLeft();
        break;
    case SDLK_RIGHT:
        m_pPlayerModel->SeekRight();
        break;
    default:
        break;
    }
}

Constants::PlayerActions PlayerController::CheckActionInput(SDL_Event eve)
{
    switch (eve.key.keysym.sym)
    {
    case SDLK_SPACE:
        return PLAYERACTION(Proceed);
    case SDLK_q:
        return PLAYERACTION(Interact);
    case SDLK_w:
        if (m_bombAbility)
            return PLAYERACTION(Bomb);
        else
            return PLAYERACTION(NoAction);
    case SDLK_ESCAPE:
        return PLAYERACTION(Quit);
    default:
        return PLAYERACTION(NoAction);
        break;
    }
}

void PlayerController::Update()
{
    m_pPlayerModel->Update();
    m_pPlayerView->Update();
}

// Have the view be set to the correct animation
void PlayerController::AnimatePlayerView()
{
    m_pPlayerView->SetAnimationFrame();
    m_pPlayerView->SetAnimationImage(m_pPlayerModel->GetFace());
}

// Have the player view render the player model
void PlayerController::Draw()
{
    m_pPlayerView->Render(m_pPlayerModel->GetX(), m_pPlayerModel->GetY());
}

void PlayerController::Pause()
{
    m_paused = true;
    m_pPlayerModel->SetIdle();
}

void PlayerController::Unpause()
{
    m_paused = false;
}

// This is typically called when the player enters a new room
void PlayerController::SetAtStartPosition(const int x, const int y)
{
    m_pPlayerModel->SetPosition(x, y);
}

CollisionShapeAABB * PlayerController::GetCollider() const
{
    return m_pPlayerView->GetCollider();
}

CollisionShapeLine * PlayerController::GetDstCollider() const
{
    return m_pPlayerModel->GetDstCollider(); 
}

// Get the direction the player is facing
Constants::MovementDirections PlayerController::GetFace() const
{
    return m_pPlayerModel->GetFace();
}