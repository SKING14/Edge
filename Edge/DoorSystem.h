#pragma once
#include "EventListener.h"

#include <vector>

class DoorModel;

class DoorSystem : public EventListener
{
private:
	std::vector<DoorModel*> m_doors;
public:
    DoorSystem() {}
    ~DoorSystem() {}
    void HandleEvent(const Event * pEvent) override;
	void OpenDoor(const unsigned int doorID);

	void AddDoorToSystem(DoorModel* door);
};

