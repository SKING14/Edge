#pragma once

#include <SDL.h>

// This is an abstract parent class for all the different collision shapes
class CollisionShape
{
public:
    CollisionShape() {}
    virtual ~CollisionShape() {}

    enum class Type
    {
        Point,
        AABB,
        Line
    };

    Type m_type;
};

