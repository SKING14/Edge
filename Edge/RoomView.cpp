#include "RoomView.h"

#include "Constants.h"
#include "Wall.h"
#include "DoorController.h"
#include "DoorModel.h"
#include "DoorView.h"
#include "CrateModel.h"

RoomView::RoomView(SDL_Renderer *pRen, std::vector<Wall> &walls, std::vector<CrateModel> crates)
    :m_pRen(pRen)
{
    LoadImages();
    SetWalls(walls);
    SetCrates(crates);
}

void RoomView::LoadImages()
{
    m_pCornerWallIMG = Image(VARIABLES("CornerWall"), m_pRen);
    m_pLeftRightWallIMG = Image(VARIABLES("LeftRightWall"), m_pRen);
    m_pUpDownWallIMG = Image(VARIABLES("UpDownWall"), m_pRen);
    m_pCrateIMG = Image(VARIABLES("Crate"), m_pRen);
    m_pFloor = Image(VARIABLES("MetalFloor"), m_pRen);
}

void RoomView::SetWalls(std::vector<Wall>& walls)
{
    int index = 0;
    for (auto wall : walls)
    {
        switch (wall.GetWallType())
        {
        case Wall::WallType::Corner:
            m_wallImages.push_back(m_pCornerWallIMG);
            break;
        case Wall::WallType::Horizontal:
            m_wallImages.push_back(m_pLeftRightWallIMG);
            break;
        case Wall::WallType::LeftVert:
        case Wall::WallType::RightVert:
            m_wallImages.push_back(m_pUpDownWallIMG);
            break;
        default:
            break;
        }
        m_wallImages[index].SetDstRect(wall.GetDimensions());
        ++index;
    }
}

void RoomView::SetCrates(std::vector<CrateModel>& crates)
{
    int index{ 0 };
    for (auto crate : crates)
    {
        m_crateImages.push_back(m_pCrateIMG);
        m_crateImages[index].SetDstRect(crate.GetDimensions());
        ++index;
    }
}

void RoomView::CrateDestroyed(int index)
{
    m_crateImages.erase(m_crateImages.begin() + index);
}

void RoomView::Render()
{
    m_pFloor.FillScreen();
    RenderGroup(m_wallImages);
    RenderGroup(m_crateImages);
}

void RoomView::RenderDoors(std::vector<DoorController*> &doors)
{
    for (auto door : doors)
    {
        door->Update();
        door->Draw();
    }
}
