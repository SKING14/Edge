#pragma once
#include "View.h"

#include "CollisionShapeAABB.h"
#include "Constants.h"
#include "Image.h"

class PlayerView : public View
{
private:
    SDL_Renderer* m_pRen{ nullptr };
    Image m_pPlayer;

    // Animation variables
    int m_srcX;
    int m_srcY;
    int m_currentFrame{ 0 };
    int m_timelineLength;

    // Collider
    CollisionShapeAABB *m_pCollider;

public:
    PlayerView(SDL_Renderer* pRen, std::string variables);
    ~PlayerView();

    void SetAnimationImage(Constants::MovementDirections facing);
    void SetAnimationFrame();
    void Render(int xPos, int yPos);
    CollisionShapeAABB* GetCollider() const { return m_pCollider; }
    void Update() override;

private:
    void Init(std::string variables);

};