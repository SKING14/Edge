#pragma once

#include <SDL.h>
#include "Image.h"

class CollisionShapeAABB;

class Trigger
{
private:
    CollisionShapeAABB *m_pCollider;
    Image m_pImage;

public:
    Trigger(const char* pFilename, SDL_Renderer* pRen);
    ~Trigger();
    void Render();
    CollisionShapeAABB * GetCollider() const { return m_pCollider; }
};