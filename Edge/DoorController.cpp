#include "DoorController.h"

#include <string>
#include "TextFileReader.h"
#include "RoomController.h"
#include "Trigger.h"
#include "ButtonView.h"
#include "DoorModel.h"
#include "DoorView.h"
#include "CollisionShapeAABB.h"

DoorController::DoorController(SDL_Renderer *pRen, const char *pFilename, int eventID)
{
    Init(pRen, pFilename, eventID);
}

void DoorController::Init(SDL_Renderer *pRen, const char *pFilename, int eventID)
{
    std::string filenames;
    TextFileReader::ReadEntireTextToString(pFilename, filenames);
    std::string buttonTrigger = TextFileReader::GetSelectedDataFromString(filenames, "Button Trigger: ");
    const char *pButton = buttonTrigger.c_str();
    
    std::string enterTrigger = TextFileReader::GetSelectedDataFromString(filenames, "Enter Trigger: ");
    const char *pEnter = enterTrigger.c_str();

    std::string doorModel = TextFileReader::GetSelectedDataFromString(filenames, "Door Model: ");
    const char *pDoorModel = doorModel.c_str();

    m_doorButtonTrigger = new Trigger(pButton, pRen);
    m_enterRoomTrigger = new Trigger(pEnter, pRen);
    m_doorButton = new ButtonView(VARIABLES("Button"), pRen, m_doorButtonTrigger->GetCollider()->GetRect());
    m_doorModel = new DoorModel(pDoorModel, eventID);
    m_doorView = new DoorView(VARIABLES("DoorAnimation"), pRen, m_doorModel->GetDimensions());
}

Constants::PlayerActions DoorController::Input()
{
    return Constants::PlayerActions();
}

DoorController::~DoorController()
{
    delete m_doorButton;
    m_doorButton = nullptr;
    delete m_doorButtonTrigger;
    m_doorButtonTrigger = nullptr;
    delete m_enterRoomTrigger;
    m_enterRoomTrigger = nullptr;
    delete m_doorModel;
    m_doorModel = nullptr;
    delete m_doorView;
    m_doorView = nullptr;
}

void DoorController::Update()
{
    m_doorView->Update(m_doorModel->CheckIfOpen());
}

void DoorController::Draw()
{
    m_doorView->Render();
    m_doorButton->Render();
}

CollisionShapeAABB * DoorController::GetCollider()
{
    return m_doorModel->GetCollider();
}

bool DoorController::CheckIfOpen()
{
    return m_doorModel->CheckIfOpen();
}

DoorModel* DoorController::GetModel()
{
    return m_doorModel;
}

Trigger* DoorController::GetEnterTrigger()
{
    return m_enterRoomTrigger;
}

Trigger * DoorController::GetButtonTrigger()
{
    return m_doorButtonTrigger;
}