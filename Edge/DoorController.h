#pragma once
#include "Controller.h"

class CollisionShape;
class ButtonView;
class Trigger;
class DoorModel;
class DoorView;

#include "Constants.h"
#include "CollisionShapeAABB.h"

class DoorController : public Controller
{
private:
    DoorModel* m_doorModel;
    DoorView* m_doorView;
    Trigger* m_enterRoomTrigger;
    Trigger* m_doorButtonTrigger;
    ButtonView* m_doorButton;

public:
    DoorController(SDL_Renderer * pRen, const char * pFilename, int eventID);
    ~DoorController();

    DoorModel* GetModel();
    Trigger* GetEnterTrigger();
    Trigger* GetButtonTrigger();
    CollisionShapeAABB* GetCollider();
    bool CheckIfOpen();
    void Update() override;
    void Draw() override;

private:
    void Init(SDL_Renderer * pRen, const char * pFilename, int eventID);

   Constants::PlayerActions Input() override;
};

