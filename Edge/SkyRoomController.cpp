#include "SkyRoomController.h"

#include "HallwayEvent.h"
#include "ButtonEvent.h"
#include "MasterSystem.h"
#include "EventIDManager.h"

SkyRoomController::SkyRoomController(EventSystem * pEventSystem, SDL_Renderer * pRen, PlayerController * pPlayerController, MasterSystem * pMasterSytem)
    : RoomController(pEventSystem, pRen, pPlayerController, VARIABLES("SkyRoomWalls"), pMasterSytem)
    , m_rightWeightDoorController(pRen, VARIABLES("SkyToRightWeightController"), ID(SkyToRightWeightButtonEventID))
 //   , m_gameOver(VARIABLES("GameOverTrigger"), pRen)
{
    Init();
}

SkyRoomController::~SkyRoomController()
{
    CleanupEvents();
}

void SkyRoomController::Init()
{
    SetRoom(RoomController::SkyRoom);
    m_pMasterSystem->AddRoom(this);
    m_pMasterSystem->AddDoor(m_rightWeightDoorController.GetModel());
    m_doors.push_back(&m_rightWeightDoorController);
    AddEvents();
    AddListeners();
    GenerateTriggers();
}

void SkyRoomController::CleanupEvents()
{
    delete m_pEnterRightWeightEvent;
    m_pEnterRightWeightEvent = nullptr;
    delete m_pRightWeightButtonTriggerEvent;
    m_pRightWeightButtonTriggerEvent = nullptr;
    
    m_pEventSystem->RemoveListener(ID(EnterRightWeightFromSkyEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(EnterRightWeightFromSkyEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(EventIDManager::SkyToRightWeightButtonEventID, m_pMasterSystem->GetDoorSystem());
}

void SkyRoomController::CheckDialogueBoxes()
{
}

void SkyRoomController::RenderUniqueRoomObjects()
{
}

void SkyRoomController::AddEvents()
{
    m_pEnterRightWeightEvent = new HallwayEvent(ID(EnterRightWeightFromSkyEventID));
    m_pRightWeightButtonTriggerEvent = new ButtonEvent(ID(SkyToRightWeightButtonEventID));
}

void SkyRoomController::AddListeners()
{
    m_pEventSystem->AddListener(ID(EnterRightWeightFromSkyEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(EnterRightWeightFromSkyEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(EventIDManager::SkyToRightWeightButtonEventID, m_pMasterSystem->GetDoorSystem());
}

void SkyRoomController::TriggerProceedEvents()
{
}

void SkyRoomController::GenerateTriggers()
{
    m_triggers.insert(std::make_pair(m_pEnterRightWeightEvent, m_rightWeightDoorController.GetEnterTrigger()));
    m_buttons.insert(std::make_pair(m_pRightWeightButtonTriggerEvent, m_rightWeightDoorController.GetButtonTrigger()));
}
