#pragma once
#include "RoomController.h"

#include "Trigger.h"
#include "DialogueBox.h"
#include "DoorController.h"

class DialogueBoxSystem;
class DialogueBoxEvent;
class HallwayEvent;
class ButtonEvent;
class PlayerEvent;

// Starting level of the game
class PlayersRoomController : public RoomController
{
private:
    // Event Variables
    DialogueBoxEvent *m_pExitDialogueBoxEvent{ nullptr };
    PlayerEvent *m_pPlayerProceedEvent{ nullptr };
    DoorController m_recDoorController;
    HallwayEvent *m_pEnterRecRoomEvent{ nullptr };
    ButtonEvent *m_pRecDoorButtonEvent{ nullptr };
    
    // DialogueBox
    DialogueBox m_pIntro;
    Trigger m_pIntroTrigger;
    DialogueBoxSystem *m_pDialogueBoxSystem{ nullptr };
    DialogueBoxEvent *m_pIntroTriggerEnterEvent{ nullptr };

public:
    PlayersRoomController(EventSystem* pEventSystem, SDL_Renderer* pRen, 
        PlayerController* pPlayerController, MasterSystem *pMasterSystem);
    ~PlayersRoomController() override;
    
private:
    void Init() override;
    void CleanupEvents() override;

    void RenderUniqueRoomObjects() override;
    void GenerateTriggers() override;
    void AddEvents() override;
    void AddListeners() override;
    void CheckDialogueBoxes() override;
    void TriggerProceedEvents() override;


};