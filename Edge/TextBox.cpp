#include "TextBox.h"

#include "TextFileReader.h"

#include <cassert>

TextBox::TextBox(const char *pFilename, SDL_Renderer* pRen)
    : m_pRen(pRen)
{
    Init(pFilename);
}

void TextBox::Init(const char *pFilename)
{
    std::string entireText;
    std::string variable;
    TextFileReader::ReadEntireTextToString(pFilename, entireText);

    variable = TextFileReader::GetSelectedDataFromString(entireText, "Font: ");
    m_pFontFile += variable;

    variable = TextFileReader::GetSelectedDataFromString(entireText, "Text: ");
    m_pTextFile += variable;

    variable = TextFileReader::GetSelectedDataFromString(entireText, "Font Size: ");
    TextFileReader::GetValueFromString(variable, 0, m_fontSize);

    TextFileReader::GetColorFromString(entireText, m_textColor, (std::string)"Text Color: ");
    LoadFont();
}

void TextBox::LoadFont()
{
    const char *fontFile = m_pFontFile.c_str();
    m_pFont = TTF_OpenFont(fontFile, m_fontSize);
    assert(m_pFont != nullptr);
}

