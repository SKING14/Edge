#include "GameStatePlay.h"

#include "GameStateMachine.h"
#include "EventSystem.h"
#include "PlayersRoomController.h"
#include "RecRoomController.h"
#include "WeightRoomController.h"
#include "SkyRoomController.h"


GameStatePlay::GameStatePlay(EventSystem* pEventSystem, SDL_Renderer* pRen)
    : m_pEventSystem(pEventSystem)
    , m_pRen(pRen)
    , m_playerController(m_pRen)
    , m_masterSystem(&m_playerController)
{
}

GameStatePlay::~GameStatePlay()
{
    delete m_pCurrentRoom;
    m_pCurrentRoom = nullptr;
}

void GameStatePlay::Enter(GameStateMachine* gsm)
{
    GameState::Enter(gsm);
    m_pCurrentRoom = new PlayersRoomController(m_pEventSystem, m_pRen, &m_playerController, &m_masterSystem);
}

bool GameStatePlay::Update()
{
	while (m_currentRoomName == m_newRoom)
    {
        m_newRoom = m_pCurrentRoom->Update();
        m_isPlaying = m_pCurrentRoom->CheckIfQuit();
        if (!m_isPlaying)
            break;
        SDL_RenderPresent(m_pRen);
    }
	
    SetNewRoom();
	
    return m_isPlaying;
}

void GameStatePlay::SetNewRoom()
{
    if (m_pCurrentRoom != nullptr)
    {
        delete m_pCurrentRoom;
        m_pCurrentRoom = nullptr;
    }

    switch (m_newRoom)
    {
    case ROOM(PlayersRoom):
        m_pCurrentRoom = new PlayersRoomController(m_pEventSystem, m_pRen, &m_playerController, &m_masterSystem);
        break;
    case ROOM(RecRoom):
        m_pCurrentRoom = new RecRoomController(m_pEventSystem, m_pRen, &m_playerController, &m_masterSystem);
        break;
    case ROOM(WeightRoom):
        m_pCurrentRoom = new WeightRoomController(m_pEventSystem, m_pRen, &m_playerController, &m_masterSystem);
        break;
    case ROOM(SkyRoom):
        m_pCurrentRoom = new SkyRoomController(m_pEventSystem, m_pRen, &m_playerController, &m_masterSystem);
        break;
    default:
        break;
    }
    m_currentRoomName = m_newRoom;
}


void GameStatePlay::Exit()
{
}