#pragma once

#include "EventIDManager.h"

class Event
{
protected:
    unsigned int m_ID;

public:
    Event(unsigned int ID) : m_ID(ID) {  }
    virtual ~Event() {};

    unsigned int GetID() const { return m_ID; }

};