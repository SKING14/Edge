#include "BombModel.h"

#include "CollisionShapeAABB.h"

BombModel::BombModel(SDL_Rect dst)
{
    m_pCollider = new CollisionShapeAABB(dst);
}


BombModel::~BombModel()
{
    delete m_pCollider;
    m_pCollider = nullptr;
}

void BombModel::SetInactive()
{
    m_active = false;
}