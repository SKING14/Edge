#pragma once

class CollisionShape;
class CollisionShapeAABB;
class CollisionShapePoint;
class CollisionShapeLine;

// This class checks for collisions based on the type of collision shape
class CollisionSystem
{
public:
    CollisionSystem() {}
    ~CollisionSystem() {}

    static bool AreOverlapping(CollisionShape *a, CollisionShape *b);
    static bool AABBOverlapAABB(CollisionShapeAABB * pColliderA, CollisionShapeAABB * pColliderB);
    static bool PointOverAABB( CollisionShapePoint * point, CollisionShapeAABB * AABB);
    static bool LineOverAABB( CollisionShapeLine * line, CollisionShapeAABB * AABB);
};