#pragma once
#include "EventListener.h"

class SelectionBox;

class SelectionBoxSystem : public EventListener
{
private:
    SelectionBox* m_pSelectionBox{ nullptr };

public:
    SelectionBoxSystem(SelectionBox* pBox);
    ~SelectionBoxSystem() {}

    void HandleEvent(const Event* pEvent) override;
};