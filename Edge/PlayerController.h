#pragma once

#include "Controller.h"

#include "CollisionShapeAABB.h"
#include "CollisionShapeLine.h"

class PlayerModel;
class PlayerView;
class CollisionShape;

class PlayerController : public Controller
{
private:
    bool m_paused{ false };
    bool m_bombAbility{ true };

    PlayerModel *m_pPlayerModel;
    PlayerView *m_pPlayerView;
public:
    PlayerController(SDL_Renderer *pRen);
    ~PlayerController() override;

    Constants::PlayerActions Input();
    void Update() override;
    void Draw() override;
    void Pause();
    void Unpause();
    void SetAtStartPosition(const int x, const int y);

    // Mutators
    CollisionShapeAABB* GetCollider() const;
    CollisionShapeLine* GetDstCollider() const;
    Constants::MovementDirections GetFace() const;

private:
    void Init(SDL_Renderer *pRen);
    void CheckMovementInput(SDL_Event eve);
    Constants::PlayerActions CheckActionInput(SDL_Event eve);
    void AnimatePlayerView();
};