#pragma once

#include "CollisionShapeAABB.h"

#define WALL(x) Wall::WallType::x

class Wall
{
public:

    enum WallType
    {
        Corner = 'C',
        LeftVert = 'L',
        RightVert = 'R',
        Horizontal = 'H',
    };

    WallType m_wallType;
private:
    CollisionShapeAABB* m_pCollider{ nullptr };
public:
    Wall(WallType type, int xPos, int yPos);
    ~Wall();

    void SetWallType(WallType type) { m_wallType = type; }

    CollisionShapeAABB* GetCollider() const { return m_pCollider; }
    WallType GetWallType() const { return m_wallType; }
    SDL_Rect GetDimensions() const { return m_pCollider->GetRect(); }
private:
    void GetVariablesBasedOnType(SDL_Rect& colliderDimensions);
};