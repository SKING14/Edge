#pragma once

#include "Constants.h"
#include "EventSystem.h"
#include "RoomModel.h"
#include "RoomView.h"
#include "PlayerEvent.h"

class MasterSystem;
class PlayerController;
class Trigger;
class DoorController;
class BombModel;
class BombView;
class DialogueBox;


#define ROOM(x) RoomController::RoomName::x

// The game is made up of multiple rooms, each room has it's own game objects and events
class RoomController
{
public:
    
    enum RoomName
    {
        PlayersRoom,
        RecRoom,
        WeightRoom,
        SkyRoom
    };

protected:
    MasterSystem *m_pMasterSystem;
    SDL_Renderer *m_pRen{ nullptr };
    EventSystem *m_pEventSystem{ nullptr };
    PlayerController* m_pPlayerController{ nullptr };
    std::vector<DoorController*> m_doors;
	std::unordered_map<Event*, Trigger*> m_buttons;
	std::unordered_map<Event*, Trigger*> m_triggers;
    std::unordered_map<BombModel*, BombView*> m_bombs;
    std::vector<DialogueBox*> m_dialogueBoxes;
    PlayerEvent m_playerSetBombEvent;
    Event m_doorOpenEvent;
    RoomModel m_roomModel;
    RoomView m_roomView;
    RoomName m_currentRoom;
    Constants::PlayerActions m_playerAction;
    BombModel *m_pbombModel;
    BombView *m_pbombView;
    int m_maxBombCount{ 5 };
    bool m_isPlaying{ true };

public:
    RoomController(EventSystem* pEventSystem, SDL_Renderer* pRen, PlayerController* pPlayerController, 
        const char* pFilename, MasterSystem *pMasterSystem);
    virtual ~RoomController() {}

    RoomName Update();
    void SetRoom(RoomName newRoom) { m_currentRoom = newRoom; }
    bool CheckIfQuit() { return m_isPlaying; }

protected:
    void Render();
    void CheckTriggers();
	void CheckButtons();
    bool CheckDoors();
    void CheckBombs();
    void CheckExplosion(CollisionShapeAABB * pCollider);
    void PlayerUpdate();
    void CheckPlayerAction();
    void SpawnBomb();
    void SetGlobalEvents();

    virtual void Init() = 0;
    virtual void CleanupEvents() = 0;
    virtual void CheckDialogueBoxes() = 0;
    virtual void RenderUniqueRoomObjects() = 0;
    virtual void AddEvents() = 0;
    virtual void AddListeners() = 0;
    virtual void TriggerProceedEvents() = 0;
    virtual void GenerateTriggers() = 0;

    template <typename T, typename U>
    void RenderGroup(std::unordered_map<T, U> group)
    {
        for (auto object = group.begin(); object != group.end(); ++object)
        {
            object->second->Render();
        }
    };

private:
    SDL_Rect CalculateBombPosition();

};