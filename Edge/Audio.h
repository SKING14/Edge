#pragma once

// Music and SFX should be audio child classes
class Audio
{
public:
    Audio() {}
    virtual ~Audio() {}
    virtual void Play() = 0;
};