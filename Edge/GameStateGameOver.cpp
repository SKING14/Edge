#include "GameStateGameOver.h"


GameStateGameOver::GameStateGameOver()
{
}


GameStateGameOver::~GameStateGameOver()
{
}

void GameStateGameOver::Enter(GameStateMachine* gsm)
{
    GameState::Enter(gsm);
}

bool GameStateGameOver::Update()
{
    return m_isPlaying;
}

void GameStateGameOver::Exit()
{
}
