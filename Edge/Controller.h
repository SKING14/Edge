#pragma once

#include <SDL.h>
#include "Constants.h"

class Model;
class View;
class CollisionShape;

class Controller
{
public:
    Controller() {}
    virtual ~Controller() {}

    virtual Constants::PlayerActions Input() = 0;
    virtual void Update() = 0;
    virtual void Draw() = 0;
};