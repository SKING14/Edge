#include "DoorSystem.h"

#include "Event.h"
#include "EventIDManager.h"
#include "DoorModel.h"

void DoorSystem::HandleEvent(const Event *pEvent)
{
	OpenDoor(pEvent->GetID());
}

void DoorSystem::OpenDoor(const unsigned int doorID)
{
	for (DoorModel *door : m_doors)
	{
        if (door->GetID() == doorID)
            door->Open();
	}
}

void DoorSystem::AddDoorToSystem(DoorModel * door)
{
	m_doors.push_back(door);
}
