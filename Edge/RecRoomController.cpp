#include "RecRoomController.h"

#include "DoorController.h"
#include "MasterSystem.h"
#include "PlayerController.h"
#include "ButtonEvent.h"
#include "EventIDManager.h"
#include "HallwaySystem.h"
#include "HallwayEvent.h"
#include "PlayerSystem.h"

RecRoomController::RecRoomController(EventSystem * pEventSystem, SDL_Renderer * pRen, PlayerController * pPlayerController, MasterSystem * pMasterSystem)
	: RoomController(pEventSystem, pRen, pPlayerController, VARIABLES("RecRoomWalls"), pMasterSystem)
    , m_playerDoorController(pRen, VARIABLES("RecToPlayerController"), EventIDManager::RecToPlayerButtonEventID)
    , m_weightDoorController(pRen, VARIABLES("RecToWeightController"), EventIDManager::RecToWeightButtonEventID)
{
    Init();
}


void RecRoomController::Init()
{
    SetRoom(RoomController::RecRoom);
    m_pMasterSystem->AddRoom(this);
    m_pMasterSystem->AddDoor(m_playerDoorController.GetModel());
    m_pMasterSystem->AddDoor(m_weightDoorController.GetModel());
    m_doors.push_back(&m_playerDoorController);
    m_doors.push_back(&m_weightDoorController);
    AddEvents();
    AddListeners();
    GenerateTriggers();
}

RecRoomController::~RecRoomController()
{
    CleanupEvents();
}

void RecRoomController::RenderUniqueRoomObjects()
{
}

void RecRoomController::GenerateTriggers()
{
    m_triggers.insert(std::make_pair(m_pEnterPlayersRoomEvent, m_playerDoorController.GetEnterTrigger()));
    m_triggers.insert(std::make_pair(m_pEnterWeightRoomEvent, m_weightDoorController.GetEnterTrigger()));
    m_buttons.insert(std::make_pair(m_pRecToPlayerButtonEvent, m_playerDoorController.GetButtonTrigger()));
    m_buttons.insert(std::make_pair(m_pRecToWeightButtonEvent, m_weightDoorController.GetButtonTrigger()));
}

void RecRoomController::CleanupEvents()
{
    delete m_pEnterPlayersRoomEvent;
    m_pEnterPlayersRoomEvent = nullptr;
    delete m_pEnterWeightRoomEvent;
    m_pEnterWeightRoomEvent = nullptr;
    delete m_pRecToPlayerButtonEvent;
    m_pRecToPlayerButtonEvent = nullptr;
    delete m_pRecToWeightButtonEvent;
    m_pRecToWeightButtonEvent = nullptr;
    
    m_pEventSystem->RemoveListener(ID(EnterPlayersFromRecEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(ID(EnterPlayersFromRecEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(EnterWeightFromRecEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->RemoveListener(ID(EnterWeightFromRecEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->RemoveListener(ID(RecToPlayerButtonEventID), m_pMasterSystem->GetDoorSystem());
    m_pEventSystem->RemoveListener(ID(RecToWeightButtonEventID), m_pMasterSystem->GetDoorSystem());
}

void RecRoomController::TriggerProceedEvents()
{
}

void RecRoomController::AddEvents()
{
    m_pEnterPlayersRoomEvent = new HallwayEvent(ID(EnterPlayersFromRecEventID));
    m_pEnterWeightRoomEvent = new HallwayEvent(ID(EnterWeightFromRecEventID));
    m_pRecToPlayerButtonEvent = new ButtonEvent(ID(RecToPlayerButtonEventID));
    m_pRecToWeightButtonEvent = new ButtonEvent(ID(RecToWeightButtonEventID));
}

void RecRoomController::AddListeners()
{
    m_pEventSystem->AddListener(ID(EnterPlayersFromRecEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(ID(EnterPlayersFromRecEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(EnterWeightFromRecEventID), m_pMasterSystem->GetHallwaySystem());
    m_pEventSystem->AddListener(ID(EnterWeightFromRecEventID), m_pMasterSystem->GetPlayerSystem());
    m_pEventSystem->AddListener(ID(RecToPlayerButtonEventID), m_pMasterSystem->GetDoorSystem());
    m_pEventSystem->AddListener(ID(RecToWeightButtonEventID), m_pMasterSystem->GetDoorSystem());
}
