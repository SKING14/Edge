#pragma once
#include "Event.h"

class PlayerEvent : public Event
{
public:
    PlayerEvent(unsigned int ID) : Event(ID) {}
    ~PlayerEvent() {}
};