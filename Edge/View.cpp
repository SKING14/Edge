#include "View.h"

#include <cassert>

#include "AnimationStation.h"
#include "Image.h"

View::View(const char *pFilename, SDL_Renderer *pRen, SDL_Rect dimensions)
{
    AnimationStation::CreateAnimation(pRen, pFilename, m_timeline, dimensions, m_timePerFrame);
    assert(m_timeline.size() > 0);
}

void View::CreateAnimationFromMultipleSources(const char *pFilename, SDL_Renderer *pRen, SDL_Rect dimensions)
{
    AnimationStation::CreateAnimationFromMultipleSources(pRen, pFilename, m_timeline, dimensions, m_timePerFrame);
    assert(m_timeline.size() > 0);
}

void View::Render()
{
    Uint32 changeAnimation = SDL_GetTicks();
    // Warning: do not use indirection to make change this Uint32 to int
    if (changeAnimation > m_myTimer + ((m_currentAnimation + 1) * m_timePerFrame))
    {
        if (m_currentAnimation < static_cast<int>(m_timeline.size()) - 1)
            ++m_currentAnimation;
        m_timeline[m_currentAnimation].Render();
    }
    else
    {
        m_timeline[m_currentAnimation].Render();
    }
}