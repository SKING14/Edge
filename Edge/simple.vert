#version 330

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 in_uv;

// Uniforms are how we send data to a shader
// We name the uniform in the shader and then get the UniformID from OpenGL
uniform mat4 MVP;
uniform sampler2D Texture;

// Shaders can take variables in, and output other variables as well
out vec3 fragment_color;

void main()
{
	// gl_Position: vec4
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);

	//fragment_color = vec3(1, 1, 1);

	fragment_color = texture(Texture, in_uv).rgb;
}