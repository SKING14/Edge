#include "HallwaySystem.h"

#include "Event.h"
#include "RoomController.h"
#include "EventIDManager.h"

void HallwaySystem::HandleEvent(const Event* pEvent)
{
	switch (pEvent->GetID())
	{
    case EventIDManager::EnterRecFromPlayersEventID:
    case EventIDManager::EnterRecFromWeightEventID:
        m_pRoom->SetRoom(RoomController::RoomName::RecRoom);
        break;
    case EventIDManager::EnterPlayersFromRecEventID:
        m_pRoom->SetRoom(RoomController::RoomName::PlayersRoom);
        break;
    case EventIDManager::EnterWeightFromRecEventID:
    case EventIDManager::EnterRightWeightFromSkyEventID:
        m_pRoom->SetRoom(RoomController::RoomName::WeightRoom);
        break;
    case EventIDManager::EnterRightSkyFromWeightEventID:
        m_pRoom->SetRoom(RoomController::RoomName::SkyRoom);
	default:
		break;
	}
}