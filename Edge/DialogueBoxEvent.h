#pragma once
#include "Event.h"

class DialogueBoxEvent : public Event
{
public:
    DialogueBoxEvent(unsigned int ID) : Event(ID) {}
    ~DialogueBoxEvent() {}
};