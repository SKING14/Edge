#pragma once
#include "Audio.h"

#include <SDL_mixer.h>

class BgMusic : public Audio
{
private:
    Mix_Chunk *m_pBgMusic;

public:
    BgMusic(char* pFilename);
    ~BgMusic() {}

    void Play() override;
};