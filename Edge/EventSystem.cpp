#include "EventSystem.h"
#include "Event.h"
#include "EventListener.h"

void EventSystem::AddListener(EventID id, EventListener* pListener)
{
    // Search for the eventID within our map of events
    auto it = m_listeners.find(id);

    // If we found a match for the Event add the eventListener to the list of listeners to this event
    if (it != m_listeners.end())
    {
        it->second.push_back(pListener);
    }
    // Otherwise, there is no event with this ID in our map, so add the Event to our map and add the listener as a listener for this event ID
    else
    {
        ListenerList list;
        list.push_back(pListener);
        m_listeners.insert(std::make_pair(id, list));
    }
}

void EventSystem::RemoveListener(EventID id, EventListener * listener)
{
    // Search our map of events with a matching ID
    auto it = m_listeners.find(id);

    // If we found a match remove the Event
    if (it != m_listeners.end())
    {
        // Access the the list of listeners to this Event
        ListenerList *listenersForEvent = &it->second;
        
        // Loop through the list and remove all the listeners to this Event
        for (auto listIt = listenersForEvent->begin(); listIt != listenersForEvent->end(); ++listIt)
        {
            if (*listIt == listener)
            {
                listenersForEvent->erase(listIt);
                break;
            }
        }
    }
}

void EventSystem::TriggerEvent(const Event * pEvent)
{
    // See if this Event ID exist in the map
    auto it = m_listeners.find(pEvent->GetID());

    // If we found the event, loop through all the listeners for this Event and have them handle the event
    if (it != m_listeners.end())
    {
        for (auto listener : it->second)
        {
            listener->HandleEvent(pEvent);
        }
    }
}