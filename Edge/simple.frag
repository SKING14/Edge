#version 330

out vec3 color;
in vec3 fragment_color;

void main()
{
	color = fragment_color;
}