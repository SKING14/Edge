#pragma once

class StateMachine
{
public:
    StateMachine() {}
    virtual ~StateMachine() {}

    virtual bool Update() = 0;
};