#pragma once

#include <SDL.h>
#include "CollisionShapeAABB.h"

class Model
{
protected:
    CollisionShapeAABB *m_pCollider;
public:
    Model() {}
    virtual ~Model() {}

    CollisionShapeAABB* GetCollider() const { return m_pCollider; }
    SDL_Rect GetDimensions() const { return m_pCollider->GetRect(); }
};