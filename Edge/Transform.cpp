#include "Transform.h"


glm::mat4 Transform::GetModelMatrix()
{
    glm::mat4 result = m_modelMatrix;

    if (m_pParent != nullptr)
    {
        result = GetModelMatrix() * result;
    }

    return result;
}