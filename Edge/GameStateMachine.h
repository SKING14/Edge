#pragma once
#include "StateMachine.h"

#include <SDL.h>

class EventSystem;
class GameState;

class GameStateMachine : public StateMachine
{
private:
    GameState* m_pCurState{ nullptr };
    EventSystem* m_pEventSystem{ nullptr };
    SDL_Renderer* m_pRen{ nullptr };
public:
    GameStateMachine(EventSystem* pEventSystem, SDL_Renderer* pRen);
    ~GameStateMachine();

    // Different game states
    enum class State
    {
        Menu,
        Play,
        GameOver,
        GL
    };

    void ChangeState(State newState);
    bool Update();
};