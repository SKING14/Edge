#pragma once

class GameStateMachine;

class GameState
{
protected:
    bool m_isPlaying{ true };
    GameStateMachine *m_pStateMachine;
public:
    GameState() {}
    virtual ~GameState() {}

    virtual void Enter(GameStateMachine *pGSM) { m_pStateMachine = pGSM; }
    virtual bool Update() = 0;
    virtual void Exit() = 0;
};