#pragma once
#include "GameState.h"

class GameStateGameOver : public GameState
{
public:
    GameStateGameOver();
    ~GameStateGameOver();

    void Enter(GameStateMachine * gsm);

    bool Update() override;
    void Exit() override;
};