#pragma once
#include "View.h"

class BombView : public View
{
public:
    BombView() {}
    BombView(const char * pFilename, SDL_Renderer * pRen, SDL_Rect dimensions);
    ~BombView() {}

    bool AnimationCompleted();
};