#pragma once
#include "GameState.h"

class GameStateGL : public GameState
{
public:
    GameStateGL();
    ~GameStateGL();

    void Enter(GameStateMachine *gsm)  override;
    bool Update() override;
    void Exit() override;
};

