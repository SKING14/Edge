#pragma once
#include "CollisionShape.h"

class CollisionShapeAABB : public CollisionShape
{
private:
    SDL_Rect m_rect;

public:
    CollisionShapeAABB(SDL_Rect rect) { m_rect = rect; m_type = Type::AABB;}
    ~CollisionShapeAABB() {}

    // Mutator
    void SetRect(SDL_Rect rect) { m_rect = rect; }

    // Accessors
    SDL_Rect GetRect() const { return m_rect; }
    int GetX() const { return m_rect.x; }
    int GetY() const { return m_rect.y; }
    int GetW() const { return m_rect.w; }
    int GetH() const { return m_rect.h; }
};