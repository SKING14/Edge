#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <string>

class Image
{
protected:
    SDL_Renderer *m_pRen{ nullptr };
    SDL_Texture *m_pTexture{ nullptr };
    SDL_Rect m_src;
    SDL_Rect m_dst;
    std::string m_pImageFilename = "Assets/Images/";

public:
    Image() {}
    Image(const char* pFilename, SDL_Renderer* pRen);
    Image(const char * pTextFilename, SDL_Renderer * pRen, SDL_Rect dst);
    Image(SDL_Texture *pTexture, SDL_Renderer * pRen, SDL_Rect src, SDL_Rect dst);
    ~Image() {}

    void Render();
    void Render(int srcX, int srcY, int dstX, int dstY);
    void FillScreen();

    SDL_Rect GetDstRect() const { return m_dst; }
	void SetDstRect(SDL_Rect newDst) { m_dst = newDst; }

private:
    void LoadImage(const char *pTextFilename, std::string& entireText);
};