#pragma once
#include "Event.h"

class MenuEvent : public Event
{
public:
    MenuEvent(unsigned int ID) : Event(ID){ }
    ~MenuEvent() {}
};