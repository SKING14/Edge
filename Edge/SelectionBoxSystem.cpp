#include "SelectionBoxSystem.h"

#include "Event.h"
#include "SelectionBox.h"

SelectionBoxSystem::SelectionBoxSystem(SelectionBox* pBox)
    : m_pSelectionBox(pBox)
{
}

// Have the selection box cursor move up or down depending on if the up arrow key event 
//  or down arrow key event was triggered
void SelectionBoxSystem::HandleEvent(const Event* pEvent)
{
    switch (pEvent->GetID())
    {
        case EventIDManager::UpKeyPressedEventID:
            m_pSelectionBox->MoveUp();
            break;
        case EventIDManager::DownKeyPressedEventID:
            m_pSelectionBox->MoveDown();
            break;
        default:
            break;
    }
}