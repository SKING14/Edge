#pragma once

class Event;

class EventListener
{
public:
    EventListener() {}
    virtual ~EventListener() {}

    virtual void HandleEvent(const Event* pEvent) = 0;
};