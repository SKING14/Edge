#include "Wall.h"

#include <string>

#include "TextFileReader.h"
#include "Constants.h"


Wall::Wall(WallType type, int xPos, int yPos)
    : m_wallType(type)
{
    SDL_Rect newRect{ 0, 0, 0, 0 };
    GetVariablesBasedOnType(newRect);
    newRect.x = xPos;
    newRect.y = yPos;
    m_pCollider = new CollisionShapeAABB(newRect);
}

Wall::~Wall()
{
//    delete m_pCollider;
//    m_pCollider = nullptr;
}

void Wall::GetVariablesBasedOnType(SDL_Rect& colliderDimensions)
{
    std::string myString;

    switch (m_wallType)
    {
    case Wall::Corner:
        TextFileReader::ReadEntireTextToString(VARIABLES("CornerWall"), myString);
        break;
    case Wall::Horizontal:
        TextFileReader::ReadEntireTextToString(VARIABLES("LeftRightWall"), myString);
        break;
    case Wall::RightVert:
    case Wall::LeftVert:
        TextFileReader::ReadEntireTextToString(VARIABLES("UpDownWall"), myString);
        break;
    default:
        break;
    }
    TextFileReader::GetRecFromString(myString, colliderDimensions, (std::string)"Destination: ");
}