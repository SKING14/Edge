#include "RoomModel.h"

#include "Constants.h"
#include "CollisionSystem.h"
#include "CollisionShapeAABB.h"
#include "TextFileReader.h"
#include "Wall.h"
#include "CrateModel.h"

#include <cassert>

RoomModel::RoomModel(const char* pFilename)
{
    TextFileReader::ReadWallsTypeAndPositionForRoom(pFilename, m_wallTypes);
    assert(m_wallTypes.size() > 0);
    GenerateWalls();
}

void RoomModel::GenerateWalls()
{
    int xPos{ 0 };
    int yPos{ 0 };

    for (unsigned int index{ 0 }; index < m_wallTypes.size(); ++index)
    {
        m_walls.reserve(m_wallTypes.size() / sizeof(std::string));
        

        if (m_wallTypes[index] == "C")
        {
            m_walls.push_back(Wall(WALL(Corner), xPos, yPos));
        }
        else if (m_wallTypes[index] == "H")
        {
            m_walls.push_back(Wall(WALL(Horizontal), xPos, yPos));
        }
        else if (m_wallTypes[index] == "LV")
        {
            m_walls.push_back(Wall(WALL(LeftVert), xPos, yPos));
        }
        else if (m_wallTypes[index] == "RV")
        {
            m_walls.push_back(Wall(WALL(RightVert), xPos + Constants::k_doubleFloorSize, yPos));
        }
        else if (m_wallTypes[index] == "B")
        {
            m_crates.push_back(CrateModel(xPos, yPos));
        }
        else
        {
            assert(m_wallTypes[index] == "E");
        }
        xPos += Constants::k_doubleFloorSize + Constants::k_doubleFloorSize;
        if (xPos >= Constants::k_winWidth)
        {
            xPos = 0;
            yPos += Constants::k_doubleFloorSize;
        }
    }
}

bool RoomModel::CheckAllCollisions(CollisionShape *pCollider)
{
    return (CheckWallCollisions(pCollider) || CheckCrateCollisions(pCollider));
}

bool RoomModel::CheckWallCollisions(CollisionShape* pCollider)
{
    return CheckCollisions(pCollider, this->m_walls);
}

bool RoomModel::CheckCrateCollisions(CollisionShape *pCollider)
{
    return CheckCollisions(pCollider, this->m_crates);
}