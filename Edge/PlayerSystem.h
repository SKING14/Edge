#pragma once
#include "EventListener.h"

class PlayerController;

class PlayerSystem : public EventListener
{
private:
    PlayerController *m_pPlayer{ nullptr };
public:
    PlayerSystem(PlayerController *pPlayer);
    ~PlayerSystem() {}

    void HandleEvent(const Event * pEvent);
};