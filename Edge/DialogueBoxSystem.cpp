#include "DialogueBoxSystem.h"

#include "DialogueBox.h"
#include "Event.h"

void DialogueBoxSystem::HandleEvent(const Event* pEvent)
{
    switch (pEvent->GetID())
    {
    case EventIDManager::IntroTriggerEventID:
        m_pDialogueBox->Enable();
        break;
    case EventIDManager::PlayerProceedEventID:
        m_pDialogueBox->MarkAsRead();
        break;
    default:
        break;
    }
}