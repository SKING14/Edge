#pragma once
#include "RoomController.h"

#include "DoorController.h"
//#include "Trigger.h"

class HallwayEvent;
class ButtonEvent;

class SkyRoomController : public RoomController
{
private:
    HallwayEvent *m_pEnterRightWeightEvent;
    ButtonEvent *m_pRightWeightButtonTriggerEvent;
    DoorController m_rightWeightDoorController;

  //  Trigger m_gameOver;

public:
    SkyRoomController(EventSystem *pEventSystem, SDL_Renderer *pRen, PlayerController *pPlayerController, MasterSystem *pMasterSytem );
    ~SkyRoomController();

private:
    void Init() override;
    void CleanupEvents() override;
    void CheckDialogueBoxes() override;
    void RenderUniqueRoomObjects() override;
    void AddEvents() override;
    void AddListeners() override;
    void TriggerProceedEvents() override;
    void GenerateTriggers() override;
};