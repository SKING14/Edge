#pragma once
#include "TextBox.h"

#include <SDL_ttf.h>
#include <fstream>
#include <vector>
#include <string>

class DialogueBox : public TextBox
{
private:
    typedef std::pair<const char*, short int> SingleLine;
    typedef std::vector<SingleLine> DialogueLines;
    
    std::string m_dialogue;
    DialogueLines m_lines;
    SDL_Color m_borderColor;
    SDL_Color m_bgColor;
    SDL_Rect m_borderRect;
    SDL_Rect m_bgRect;

    int m_maxLines{ 0 };
    int m_maxCharsPerLine{ 0 };
    int m_boxCount{ 1 };
    int m_boxesRead{ 0 };

    bool m_enabled{ false };
    bool m_read{ false };
    bool m_additionalBoxRequired{ false };

public:
    DialogueBox(const char* pFilename, SDL_Renderer* pRen);
    void DialogueInit(const char * pFilename);
    ~DialogueBox();

    void SetupBox();
    void SetupLines();
    void Enable();
    void MarkAsRead();
    void Render();
    bool CheckIfRead() const;
};