#include "RoomController.h"

#include <utility>

#include "MasterSystem.h"
#include "PlayerController.h"
#include "CollisionSystem.h"
#include "CollisionShapeAABB.h"
#include "CollisionShapeLine.h"
#include "Wall.h"
#include "Trigger.h"
#include "DoorModel.h"
#include "BombModel.h"
#include "BombView.h"
#include "DoorController.h"

#include <cassert>

RoomController::RoomController(EventSystem* pEventSystem, SDL_Renderer* pRen, PlayerController* pPlayerController, const char* pFilename, MasterSystem *pMasterSystem)
    : m_pEventSystem(pEventSystem)
    , m_pRen(pRen)
    , m_pPlayerController(pPlayerController)
    , m_pMasterSystem(pMasterSystem)
    , m_roomModel(pFilename)
    , m_roomView(pRen, m_roomModel.GetWalls(), m_roomModel.GetCrates())
    , m_playerSetBombEvent(EventIDManager::PlayerSetBombEventID)
    , m_doorOpenEvent(EventIDManager::DoorOpenEventID)
{
    SetGlobalEvents();
}

void RoomController::SetGlobalEvents()
{
    m_pEventSystem->AddListener(EventIDManager::PlayerSetBombEventID, m_pMasterSystem->GetAudioSystem());
    m_pEventSystem->AddListener(EventIDManager::DoorOpenEventID, m_pMasterSystem->GetAudioSystem());
}

RoomController::RoomName RoomController::Update()
{
    PlayerUpdate();
    
    if (m_bombs.size() > 0)
        CheckBombs();
    CheckDialogueBoxes();
    CheckPlayerAction();
    CheckTriggers();

    Render();
	return m_currentRoom;
}

void RoomController::PlayerUpdate()
{
    m_playerAction = m_pPlayerController->Input();

    if (!m_roomModel.CheckAllCollisions(m_pPlayerController->GetDstCollider()))
    {
        if (CheckDoors())
        {
            m_pPlayerController->Update();
        }
    }
}

void RoomController::CheckTriggers()
{
    for (auto trigger = m_triggers.begin(); trigger != m_triggers.end();)
    {
        if (CollisionSystem::AreOverlapping(m_pPlayerController->GetDstCollider(), trigger->second->GetCollider()))
        {
            m_pEventSystem->TriggerEvent(trigger->first);
            trigger = m_triggers.erase(trigger);
        }
        else
        {
            ++trigger;
        }
    }
}

void RoomController::CheckButtons()
{
	for (auto button = m_buttons.begin(); button != m_buttons.end();)
	{
		if (CollisionSystem::AreOverlapping(m_pPlayerController->GetDstCollider(), button->second->GetCollider()))
		{
            m_pEventSystem->TriggerEvent(button->first);
            m_pEventSystem->TriggerEvent(&m_doorOpenEvent);
		}
		++button;
	}
}

bool RoomController::CheckDoors()
{
    for (unsigned int i{ 0 }; i < m_doors.size(); ++i)
    {
        if (CollisionSystem::AreOverlapping(m_pPlayerController->GetDstCollider(), m_doors[i]->GetCollider()))
        {
			return (m_doors[i]->CheckIfOpen());
        }
    }
    return true;
}

void RoomController::CheckBombs()
{
    if (!m_bombs.empty())
    {
        assert(m_bombs.size() > 0);
        for (auto bomb = m_bombs.begin(); bomb != m_bombs.end();)
        {
            if (bomb->second->AnimationCompleted())
            {
                CheckExplosion(bomb->first->GetCollider());
                delete bomb->first;
                delete bomb->second;
                m_bombs.erase(bomb->first);
                break;
            }
            if (m_bombs.empty())
                break;
            else 
                ++bomb;
        }
    }
}

void RoomController::CheckExplosion(CollisionShapeAABB *pCollider)
{
    std::vector<CrateModel>* pCrates = &m_roomModel.GetCrates();
    
    for (unsigned int i{ 0 }; i < pCrates->size(); ++i)
    {
        if (CollisionSystem::AABBOverlapAABB(pCollider, (*pCrates)[i].GetCollider()))
        {
            (*pCrates).erase((*pCrates).begin() + i);
            m_roomView.CrateDestroyed(i);
        }
    }
}

void RoomController::CheckPlayerAction()
{
    switch (m_playerAction)
    {
    case Constants::NoAction:
        break;
    case Constants::Proceed:
        TriggerProceedEvents();
        break;
    case Constants::Interact:
        CheckButtons();
        break;
    case Constants::Bomb:
        SpawnBomb();
        break;
    case Constants::Quit:
        m_isPlaying = false;
        break;
    default:
        break;
    }
}

void RoomController::SpawnBomb()
{
    if (m_bombs.size() < m_maxBombCount)
    {
        m_pbombModel = new BombModel(CalculateBombPosition());
        m_pbombView = new BombView(VARIABLES("BombAnimation"), m_pRen, m_pbombModel->GetDimensions());
        m_bombs.insert(std::make_pair(m_pbombModel, m_pbombView));
        m_pEventSystem->TriggerEvent(&m_playerSetBombEvent);
    }
}

SDL_Rect RoomController::CalculateBombPosition()
{
    SDL_Rect bombPosition = m_pPlayerController->GetCollider()->GetRect();
    Constants::MovementDirections inFrontOfPlayer = m_pPlayerController->GetFace();
    
    switch (inFrontOfPlayer)
    {
    case Constants::Up:
        bombPosition.y -= Constants::k_doubleFloorSize;
        break;
    case Constants::Down:
        bombPosition.y += Constants::k_doubleFloorSize;
        break;
    case Constants::Left:
        bombPosition.x -= Constants::k_doubleFloorSize;
        break;
    case Constants::Right:
        bombPosition.x += Constants::k_doubleFloorSize;
        break;
    default:
        break;
    }
    return bombPosition;
}

void RoomController::Render()
{
    m_roomView.Render();
    m_roomView.RenderDoors(m_doors);
    m_pPlayerController->Draw();
    RenderGroup(m_buttons);
    RenderGroup(m_triggers);
    RenderGroup(m_bombs);
    RenderUniqueRoomObjects();
}