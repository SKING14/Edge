#pragma once
#include "Event.h"

class ButtonEvent : public Event
{
public:
	ButtonEvent(unsigned int ID) : Event(ID) {}
    ~ButtonEvent() {}
};