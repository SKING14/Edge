#include "BgMusic.h"

#include <SDL_mixer.h>
#include <cassert>

#define Enable 1

BgMusic::BgMusic(char* pFilename)
{
    m_pBgMusic = Mix_LoadWAV(pFilename);
}

void BgMusic::Play()
{
#if Enable
    Mix_Volume(1, 50);
    Mix_PlayChannel(1, m_pBgMusic, 5);
#endif
}
