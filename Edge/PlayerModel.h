#pragma once
#include "Model.h"

#include "Constants.h"

#include <string>

class CollisionShapeAABB;
class CollisionShape;
class CollisionShapeLine;

class PlayerModel : public Model
{
private:
    Constants::MovementDirections m_pFacing{ MOVE(Down) };
    Constants::MovementDirections m_nextMove{ MOVE(Idle) };
    int m_xPos;
    int m_yPos;
    int m_playerSpeed;
    CollisionShapeLine *m_pDstCollider{ nullptr };

public:
    PlayerModel(std::string variables);
    void Init(std::string variables);
    ~PlayerModel() override;
    
    void Update();
    void SeekUp();
    void SeekDown();
    void SeekLeft();
    void SeekRight();

    // Mutators
    void SetIdle();
    void SetPosition(const int x, const int y);

    // Accessors
    CollisionShapeLine* GetDstCollider() const;
    int GetX() const { return m_xPos; }
    int GetY() const { return m_yPos; }
    Constants::MovementDirections GetFace() const { return m_pFacing; }
};